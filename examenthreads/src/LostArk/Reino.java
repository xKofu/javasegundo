package LostArk;

public class Reino {

	int capacidadReino = 40;
	
	Object Entrada = new Object();
	
	public Boolean PuedoEntrar() {
		synchronized (Entrada) {
			if (capacidadReino > 0) {
				capacidadReino--;
				return true;
			}
			return false;
		}
	}
	
	public void DejarHueco() {
		capacidadReino++;
	}
	
}
