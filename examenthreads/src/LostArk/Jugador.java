package LostArk;

import java.util.Random;
import java.util.concurrent.Callable;


public class Jugador implements Callable<Boolean> {

	Reino m_reino;
	Servidor m_server;
	String m_name;
	
	Boolean EntrandoReino = true;
	Boolean EntrandoServidor = true;
	
	int IntentosReino = 0;
	int IntentosServidor = 0;
	
	Random r = new Random();
	
	public Jugador(Reino reino, Servidor server, String name) {
		m_reino = reino;
		m_server = server;
		m_name = name;
	}
	
	@Override
	public Boolean call() {
		
		try {
			while (EntrandoReino) {
				
				if(m_reino.PuedoEntrar()) {
					
					// HE ENTRADO AL REINO
					
					while (EntrandoServidor) {
						
						if(m_server.PuedoEntrar()) {
							
							System.out.println("Per fi jugant a " + m_server.m_name);// HE ENTRADO AL SERVIDOR
						
							Thread.sleep(r.nextInt(90)+10);
							
							System.out.println("Ja prou per avui, deixant pla�a lliure a " + m_server.m_name);
							
							synchronized (m_reino.Entrada) {
								m_reino.DejarHueco();
								m_reino.Entrada.notifyAll();
							}
							
							synchronized (m_server.Entrada) {
								m_server.DejarHueco();
								m_server.Entrada.notifyAll();
							}
							
							EntrandoReino = false;
							EntrandoServidor = false;
							
						} else {
							
							System.out.println("Un altre cop cua a " + m_server.m_name + "?!");
							
							IntentosServidor++;
							if (IntentosServidor == 7) {
								
								System.out.println("ES IMPOSIBLE ENTRAR AL SERVIDOR, PASO");
								
								synchronized (m_reino.Entrada) {
									m_reino.DejarHueco();
									m_reino.Entrada.notifyAll();
								}
								
								synchronized (m_server.Entrada) {
									m_server.DejarHueco();
									m_server.Entrada.notifyAll();
								}
								return false;
							}
							
							synchronized (m_server.Entrada) {
								m_server.Entrada.wait();
							}
							
						}
						
					}
					
				} else {
					
					System.out.println("No puc ni entrar al joc!");
					IntentosReino++;
					if (IntentosReino == 3) {
						
						System.out.println("ES IMPOSIBLE ENTRAR AL REINO, PASO");
						
						synchronized (m_reino.Entrada) {
							m_reino.DejarHueco();
							m_reino.Entrada.notifyAll();
						}
						return false;
					}
					synchronized (m_reino.Entrada) {
						
							m_reino.Entrada.wait();
						
					}
					
				}
				
			}
			
		} catch (InterruptedException e) {
			System.out.println("Ha petado jugador");
			e.printStackTrace();
		}
		return true;
	}

	
	
}
