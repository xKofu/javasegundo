package LostArk;

public class Servidor {

	String m_name;
	int capacidadServidor = 10;
	
	Object Entrada = new Object();
	
	public Servidor(String name) {
		m_name = name;
	}
	
	public Boolean PuedoEntrar() {
		synchronized (Entrada) {
			if (capacidadServidor > 0) {
				capacidadServidor--;
				return true;
			}
			return false;
		}
	}
	
	public void DejarHueco() {
		capacidadServidor++;
	}
}
