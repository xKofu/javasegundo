package LostArk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Main {

	public static void main(String[] args) {
		
		ExecutorService executor = Executors.newCachedThreadPool();
		
		ArrayList<Future<Boolean>> futuros = new ArrayList<Future<Boolean>>();
		
		Reino R = new Reino();
		
		Servidor Zinnervale = new Servidor("Zinnervale");
		Servidor Kadan = new Servidor("Kadan");
		Servidor Nineveh = new Servidor("Nineveh");
		
		for (int i = 0; i < 20; i++) 
			futuros.add(executor.submit(new Jugador(R, Zinnervale, "JugadorZ"+(i+1))));
		
		for (int i = 0; i < 20; i++) 
			futuros.add(executor.submit(new Jugador(R, Kadan, "JugadorK"+(i+1))));
		
		for (int i = 0; i < 10; i++) 
			futuros.add(executor.submit(new Jugador(R, Nineveh, "JugadorN"+(i+1))));
		
		Collections.shuffle(futuros);	
		
//		executor.submit(Zinnervale);
//		executor.submit(Kadan);
//		executor.submit(Nineveh);
		
		
		for (Future<Boolean> futuro : futuros) {
			try {
				futuro.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		
		executor.shutdownNow();
		try {
			executor.awaitTermination(1, TimeUnit.SECONDS);
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println();
		System.out.println(" FIN ");
	}

}

