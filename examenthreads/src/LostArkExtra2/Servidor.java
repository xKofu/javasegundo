package LostArkExtra2;

import java.util.concurrent.Callable;

public class Servidor implements Callable<Boolean>{

	String m_name;
	int capacidadServidor = 10;
	
	Object Entrada = new Object();
	
	public Servidor(String name) {
		m_name = name;
	}
	
	public Boolean PuedoEntrar() {
		synchronized (Entrada) {
			if (capacidadServidor > 0) {
				capacidadServidor--;
				return true;
			}
			return false;
		}
	}
	
	public void DejarHueco() {
		capacidadServidor++;
	}

	@Override
	public Boolean call() {
		
		try {
			
			//Thread.sleep(1000);
			System.out.println("Mantenimiento en 0.2 segundos!");
			Thread.sleep(20);
			return true;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
}
