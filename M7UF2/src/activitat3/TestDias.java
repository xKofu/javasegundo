package activitat3;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestDias {

	@Test
	void test() {
		//fail("Not yet implemented");
		assertEquals("lunes2", Diadelasemana.buscar_dia(4, 10, 2021));
		assertEquals("domingo1", Diadelasemana.buscar_dia(4, 10, 2020));
		assertEquals("viernes6", Diadelasemana.buscar_dia(8, 10, 2021));
		assertEquals("lunes2", Diadelasemana.buscar_dia(15, 2, 2021));
		assertEquals("martes3", Diadelasemana.buscar_dia(2, 2, 2021));
	}

}
