package activitat3;

import java.util.ArrayList;
import java.util.Scanner;

public class Diadelasemana {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Insereix una data separada per /");
		String fecha = sc.nextLine();
		
		String[] a =  fecha.split("/");
		
		if (a.length == 3) {
			
			if (a[0] == "" || a[1] == "" || a[2] == "") {
				System.out.println("Fecha no valida");
			} else {
				try {
					int dia = Integer.parseInt(a[0]);
					int mes = Integer.parseInt(a[1]);
					int any = Integer.parseInt(a[2]);
					
					System.out.println(buscar_dia(dia,mes,any));
				} catch (Exception e) {
					System.out.println("Fecha no valida");
				}
				
			}
			
		} else {
			System.out.println("Fecha no valida");
		}
		
	}
	
	public static String buscar_dia(int dia, int mes, int any) {
		
		int h, K, J;

		boolean can29 = false;
		
		if(dia <= 0 || dia> 31) {
			System.out.println("entro en los dias fuera de rango");
		return null;	
		}
		
		if ((any % 4 == 0) && ((any % 100 != 0) || (any % 400 == 0))) 
			can29 = true;
		
		else 
			can29 = false;
		
		
		
		if(mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12 ) {
			if(dia > 31) {
				System.out.println("Mas de 31");
				return null;
			}
		}
		
		else if(mes == 2) {
			
			if(can29 == true) {
				
				if(dia > 29) {
					
					return null;
				}
			}
			else if(dia > 28) {
				return null;
			}
		}
		else {
			if(dia > 30) {
				return null;
			}
		}
		
		
		
		
		if (mes<=2) {
			mes = mes + 10;
			any = any - 1;
		}
		else{
			mes = mes - 2;
		}

		
		K = any % 100;
		J = any / 100;

		
		h = ((700 + ((26 * mes -2) /10) + dia + K + (K/4) +((J/4) + 5*J)) % 7);

		h+=1;
		
		switch(h) {
			case 0:
				return "sabado" + h;
				
	
			case 1 :
				return "domingo" + h;
				
	
			case 2 :
				return "lunes" + h;
				
	
			case 3 :
				return "martes" + h;
				
	
			case 4 :
				return "miercoles" + h;
				
	
			case 5 :
				return "jueves" + h;
		
	
			case 6 :
				return "viernes" + h;

		}

		return null;
	}	

}
