package Cosas;

import java.util.concurrent.Callable;

public class Kane implements Callable<Boolean> {

	public Object Saludar = new Object();
	int Saludos = 1;

	public synchronized Boolean DarSaludo() {

		if (Saludos > 0) {
			Saludos--;
			return true;
		}

		return false;

	}

	@Override
	public Boolean call() {

		try {

			synchronized (this) {
				this.notifyAll();
			}	
			
			synchronized (Saludar) {
				Saludar.wait();
			}

			System.out.println("Me han saludado");

		} catch (InterruptedException e) {
			System.out.println("kane falla");
			e.printStackTrace();
		}

		return true;
	}

}
