package Cosas2;

import java.util.concurrent.Callable;

public class Pesao implements Callable<Boolean> {

	Kane m_kane;

	public Pesao(Kane kane) {
		m_kane = kane;
	}

	@Override
	public Boolean call() {

		try {
		
			synchronized (m_kane) {
				m_kane.wait();
			}
			
			if (m_kane.DarSaludo()) {
				System.out.println("UN SALUDO!");
				synchronized (m_kane.Saludar) {
					m_kane.Saludar.notify();
				}
			} else {
				System.out.println("No he podido saludar:C");
			}
		} catch (InterruptedException e) {
			System.out.println("aaa");
			e.printStackTrace();
		}

		return true;
	}

}
