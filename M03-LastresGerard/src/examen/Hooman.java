package examen;

import java.util.ArrayList;
import java.util.Iterator;

import examen.actions.Action;

public class Hooman {

	private String name;
	
	private ArrayList<Action> ActionList = new ArrayList<Action>();
	
	public Hooman(String name) {
		this.name = name;
	}
	
	void dailyRoutine() {
	
		for (Action action : ActionList) {
			action.act();
		}
		
	}
		
	public ArrayList<Action> getActionList() {
		return ActionList;
	}
	
	public void setActionList(ArrayList<Action> actionList) {
		ActionList = actionList;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	
}
