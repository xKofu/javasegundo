package examen.behaviours;

import java.util.ArrayList;

import examen.Pet;

public abstract class Behaviour {

	protected Pet owner;
	
	private ArrayList<String> events = new ArrayList<String>();
	
	public abstract void behave(Object object);
	
	public Behaviour(Pet owner) {
		
		this.owner = owner;
		
	}

	public void setEvents(ArrayList<String> events) {
		this.events = events;
	}
	
	public ArrayList<String> getEvents() {
		return events;
	}		
}
