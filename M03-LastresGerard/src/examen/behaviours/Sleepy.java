package examen.behaviours;

import examen.EventManager;
import examen.Hooman;
import examen.Pet;

public class Sleepy extends Behaviour {

	public Sleepy(Pet owner) {
		super(owner);
	}

	@Override
	public void behave(Object object) {
		
		if (object instanceof Hooman) {
			System.out.println(this.owner.toString() + " goes to sleep");
			EventManager.getInstance().notifyObservers("OnSleep", this.owner);
		} else if (object instanceof Pet) {
			Pet p = (Pet) object;
			if (!p.equals(this.owner)) {
				System.out.println(this.owner.toString() + " goes to sleep");
				EventManager.getInstance().notifyObservers("OnSleep", this.owner);
			}
			
		}
		
	}

}
