package examen.behaviours;

import examen.EventManager;
import examen.Hooman;
import examen.Pet;

public class Playful extends Behaviour {

	public Playful(Pet owner) {
		super(owner);
	}

	@Override
	public void behave(Object object) {
		
		if (object instanceof Hooman) {
			Hooman h = (Hooman) object;
			System.out.println(this.owner.toString() + " plays with " + h.toString());
			EventManager.getInstance().notifyObservers("OnPlay", this.owner);
		} else if (object instanceof Pet) {
			Pet p = (Pet) object;
			if (!p.equals(this.owner)) {
				System.out.println(this.owner.toString() + " plays with " + p.toString());
				EventManager.getInstance().notifyObservers("OnPlay", this.owner);
			}
			
		}
		
	}

}
