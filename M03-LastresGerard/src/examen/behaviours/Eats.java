package examen.behaviours;

import examen.EventManager;
import examen.Hooman;
import examen.Pet;

public class Eats extends Behaviour {

	public Eats(Pet owner) {
		super(owner);
	}

	@Override
	public void behave(Object object) {
		
		if (object instanceof Hooman) {
			Hooman h = (Hooman) object;
			System.out.println(this.owner.toString() + " eats " + h.toString() + "'s food");
			EventManager.getInstance().notifyObservers("OnEat", this.owner);
		} else if (object instanceof Pet) {
			Pet p = (Pet) object;
			if (!p.equals(this.owner)) {
				System.out.println(this.owner.toString() + " eats " + p.toString() + "'s food");
				EventManager.getInstance().notifyObservers("OnEat", this.owner);
			}
			
		}
		
	}

}
