package examen;

public interface Observer {
	
	public void notifyObserver(String event, Object o);

}
