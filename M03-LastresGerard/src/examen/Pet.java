package examen;

import java.util.ArrayList;

import examen.behaviours.Behaviour;

public class Pet implements Observer{

	private String name;
	
	private ArrayList<Behaviour> BehaviourList = new ArrayList<Behaviour>();
	
	public Pet(String name) {
		this.name = name;
	}

	@Override
	public void notifyObserver(String event, Object o) {
				
		for (Behaviour behaviour : BehaviourList) {
			if (behaviour.getEvents().contains(event)) {
				if (o != this)
					behaviour.behave(o);
			}
		}
		
	}
	
	public ArrayList<Behaviour> getBehaviourList() {
		return BehaviourList;
	}
	
	public void setBehaviourList(ArrayList<Behaviour> behaviourList) {
		BehaviourList = behaviourList;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.name;
	}

}
