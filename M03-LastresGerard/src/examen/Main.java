package examen;

import examen.actions.Call;
import examen.actions.Cook;
import examen.actions.Sleep;
import examen.behaviours.Eats;
import examen.behaviours.Playful;
import examen.behaviours.Sleepy;

public class Main {

	public static void main(String[] args) {
		
		EventManager EM = EventManager.getInstance();
	
		EM.AddEvent("OnPlay");
		EM.AddEvent("OnSleep");
		EM.AddEvent("OnEat");
		EM.AddEvent("OnCall");
		EM.AddEvent("OnFood");
	
		Pet Kotaro = new Pet("Kotaro");
		Playful playful = new Playful(Kotaro);
		playful.getEvents().add("OnCall");
		Kotaro.getBehaviourList().add(playful);
		
		Sleepy sleepy = new Sleepy(Kotaro);
		sleepy.getEvents().add("OnSleep");
		Kotaro.getBehaviourList().add(sleepy);
		
		Eats eats = new Eats(Kotaro);
		eats.getEvents().add("OnFood");
		Kotaro.getBehaviourList().add(eats);
		
		Pet Kami = new Pet("Kami");
		playful = new Playful(Kami);
		playful.getEvents().add("OnCall");
		playful.getEvents().add("OnFood");
		playful.getEvents().add("OnPlay");
		Kami.getBehaviourList().add(playful);
		
		eats = new Eats(Kami);
		eats.getEvents().add("OnFood");
		Kami.getBehaviourList().add(eats);
		
		Pet Turti = new Pet("Turti");
		eats = new Eats(Turti);
		eats.getEvents().add("OnFood");
		eats.getEvents().add("OnEat");
		Turti.getBehaviourList().add(eats);
		
		Hooman Chechu = new Hooman("Chechu");
		Chechu.getActionList().add(new Call(Chechu));
		Chechu.getActionList().add(new Cook(Chechu));
		Chechu.getActionList().add(new Sleep(Chechu));
		
		Hooman Froggo = new Hooman("Froggo");
		Froggo.getActionList().add(new Call(Froggo));
		Froggo.getActionList().add(new Sleep(Froggo));
		
		EM.registerObserver("OnPlay", Kotaro);
		EM.registerObserver("OnSleep", Kotaro);
		EM.registerObserver("OnEat", Kotaro);
		EM.registerObserver("OnCall", Kotaro);
		EM.registerObserver("OnFood", Kotaro);
		
		EM.registerObserver("OnPlay", Kami);
		EM.registerObserver("OnSleep", Kami);
		EM.registerObserver("OnEat", Kami);
		EM.registerObserver("OnCall", Kami);
		EM.registerObserver("OnFood", Kami);
		
		EM.registerObserver("OnPlay", Turti);
		EM.registerObserver("OnSleep", Turti);
		EM.registerObserver("OnEat", Turti);
		EM.registerObserver("OnCall", Turti);
		EM.registerObserver("OnFood", Turti);
				
		// Se que va mal perdon
		
		Chechu.dailyRoutine();
		Froggo.dailyRoutine();
		
	}
	
	
}
