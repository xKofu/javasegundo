package examen.actions;

import examen.Hooman;

public abstract class Action {

	protected Hooman owner;
	
	public abstract void act();
	
	public Action(Hooman owner) {
		
		this.owner = owner;
		
	}
	
}
