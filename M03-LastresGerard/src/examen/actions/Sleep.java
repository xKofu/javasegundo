package examen.actions;

import examen.EventManager;
import examen.Hooman;

public class Sleep extends Action {

	public Sleep(Hooman owner) {
		super(owner);
	}

	@Override
	public void act() {
		System.out.println(this.owner.toString() + " goes to spleep");
		EventManager.getInstance().notifyObservers("OnSleep", this.owner);
	}
	
}
