package examen.actions;

import examen.EventManager;
import examen.Hooman;

public class Call extends Action {

	public Call(Hooman owner) {
		super(owner);
	}

	@Override
	public void act() {
		System.out.println(this.owner.toString() + " calls out for pets");
		EventManager.getInstance().notifyObservers("OnCall", this.owner);
	}

}
