package examen.actions;

import examen.EventManager;
import examen.Hooman;

public class Cook extends Action {

	public Cook(Hooman owner) {
		super(owner);
	}

	@Override
	public void act() {
		System.out.println(this.owner.toString() + " starts cooking");
		EventManager.getInstance().notifyObservers("OnCook", this.owner);
	}

	
	
}
