package examen;

import java.util.ArrayList;
import java.util.HashMap;

public class EventManager {
	
	private static EventManager em;
	
	HashMap<String, ArrayList<Observer>> List;
	
	private EventManager() {
		List = new HashMap<String, ArrayList<Observer>>();
	}
	
	public static EventManager getInstance() {
		
		if (em == null) {
			em = new EventManager();
		}
		
		return em;
	}
		
	public void AddEvent(String event) {
		List.put(event, new ArrayList<Observer>());
	}
	
	public void registerObserver(String event, Observer a) {

		List.get(event).add(a);
		
	}
	
	public void unregisterObserver(String event, Observer a) {

		List.get(event).remove(a);
		
	}
		
	public void notifyObservers(String event, Object Object) {
				
		for (ArrayList<Observer> o : List.values()) {
			for (Observer observer : o) {
				observer.notifyObserver(event, Object);
			}
		}
			
	}

}

