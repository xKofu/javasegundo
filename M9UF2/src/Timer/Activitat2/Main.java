package Timer.Activitat2;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

	public static void main(String[] args) {
				
		ArrayList<Thread> TL = new ArrayList<Thread>();
		
		for (int i = 0; i < 10; i++) {
			Thread T = new Thread(new Timer()); 
			TL.add(T);
		}
		
		for (Thread thread : TL) {
			thread.start();
			try {
				thread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("Finale");

	}
	
}
