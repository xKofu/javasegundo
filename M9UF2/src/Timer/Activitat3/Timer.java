package Timer.Activitat3;

import java.util.Iterator;
import java.util.Random;

public class Timer implements Runnable{

	private int LifeTime;
	Random r = new Random();
	private int aux = 0;
	
	public Timer() {
		LifeTime = r.nextInt(10)+1;
	}
	
	@Override
	public void run()
	{
		try {
			for (int i = LifeTime; i >= 0; i--) {
				
				Thread.sleep(1000);
				System.out.println("Soy el Thread " + Thread.currentThread().getId() + " y me quedan " + i + " segundos de vida");
				aux = i;
			
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("Soy el Thread " + Thread.currentThread().getId() + " y me han parado cuando me quedaban " + aux + " segundos");
		}
	}

	
}
