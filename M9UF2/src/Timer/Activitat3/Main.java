package Timer.Activitat3;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

	public static void main(String[] args) {
				
		ExecutorService executor = Executors.newCachedThreadPool();
		
		for (int i = 0; i < 10; i++) {
			executor.execute(new Timer());
//			try {
//				Thread.sleep(1000/2);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
		
		executor.shutdown();
		
		try {
			executor.awaitTermination(5, TimeUnit.SECONDS);
			executor.shutdownNow();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		
		System.out.println("Finale");

	}
	
}
