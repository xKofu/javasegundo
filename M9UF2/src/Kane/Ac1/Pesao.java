package Kane.Ac1;

import java.util.concurrent.Callable;

public class Pesao implements Callable<Boolean> {

	Kane m_kane;
	String m_nombre;

	public Pesao(Kane kane, String nombre) {
		m_kane = kane;
		m_nombre = nombre;
	}
		
	@Override
	public Boolean call() throws Exception {

		try {

			System.out.println(m_nombre + " esta esperando");

			synchronized (m_kane) {
				m_kane.wait();
			}
			
			if (!m_kane.DarSaludo()) {
				return false;
			}
			
			synchronized (m_kane.Saludado) {
				m_kane.Saludado.notify();
			}
						
			System.out.println(m_nombre + " ha saludado");
								
			return true;
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return true;
	}

}
