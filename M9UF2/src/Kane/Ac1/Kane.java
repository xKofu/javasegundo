package Kane.Ac1;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;

public class Kane implements Callable<Boolean>{

	String m_nombre;
	Random r = new Random();
	public Object Saludado = new Object();
	public ArrayList<Object> Saludos = new ArrayList<Object>();
	
	public Kane(String nombre) {
		m_nombre = nombre;
	}
	
	public synchronized boolean DarSaludo() {
		
		if (Saludos.size() > 0) {
			Saludos.remove(0);
			return true;
		}
		return false;
		
	}
	
	@Override
	public Boolean call() throws Exception {

		Thread.sleep(r.nextInt(50)+50);
		
		Saludos.add(new Object());
		
		synchronized(this){
			this.notifyAll();
			
		}

		synchronized(Saludado) {
			Saludado.wait();
		}
		
		return true;
	}

	
}
