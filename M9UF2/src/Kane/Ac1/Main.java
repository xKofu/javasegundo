package Kane.Ac1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	public static void main(String[] args) {
	
		ExecutorService executor = Executors.newCachedThreadPool();
		
		Kane Kane = new Kane("Ruben");
		
		executor.submit(new Pesao(Kane, "Pau"));
		executor.submit(new Pesao(Kane, "Eric"));
		executor.submit(new Pesao(Kane, "Arnau"));
		executor.submit(new Pesao(Kane, "Albert"));
		executor.submit(new Pesao(Kane, "Luis"));
		
		executor.submit(Kane);
				
		executor.shutdown();
		
	}
	
}
