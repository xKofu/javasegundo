package Kane.Ac2;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;

public class Kane implements Callable<Boolean>{

	String m_nombre;
	Random r = new Random();
	public Object Saludado = new Object();
	public int Saludos = 1;
	
	public Kane(String nombre) {
		m_nombre = nombre;
	}
	
	public synchronized boolean DarSaludo() {
		
		if (Saludos > 0) {
			Saludos--;
			return true;
		}
		return false;
		
	}
	
	@Override
	public Boolean call() {

		try {
			Thread.sleep(r.nextInt(50)+50);
			
			synchronized(this){
				this.notifyAll();	
			}
	
			while (true) {
				synchronized(Saludado) {
					Saludado.wait();
				
					synchronized (this) {
						Saludos++;
						this.notifyAll();
					}
					
				}
				
			}
			
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println();
			System.out.println("YA ESTA BIEN DE SALUDAR HOMBRE");
		}
		
		return true;
	}

	
}
