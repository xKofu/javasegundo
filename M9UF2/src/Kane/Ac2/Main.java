package Kane.Ac2;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Main {

	public static void main(String[] args) {
	
		ArrayList<Future<Boolean>> Futuros = new ArrayList<>();
		
		ExecutorService executor = Executors.newCachedThreadPool();
		
		Kane Kane = new Kane("Ruben");
		
		Futuros.add(executor.submit(new Pesao(Kane, "Pau")));
		Futuros.add(executor.submit(new Pesao(Kane, "Eric")));
		Futuros.add(executor.submit(new Pesao(Kane, "Arnau")));
		Futuros.add(executor.submit(new Pesao(Kane, "Albert")));
		Futuros.add(executor.submit(new Pesao(Kane, "Luis")));
		
		executor.submit(Kane);		
		executor.shutdown();
		
		for (Future<Boolean> future : Futuros) {
			
			try {
				future.get();
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		executor.shutdownNow();
		try {
			executor.awaitTermination(1, TimeUnit.SECONDS);
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}

		
	}
	
}
