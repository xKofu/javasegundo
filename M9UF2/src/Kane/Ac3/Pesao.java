package Kane.Ac3;

import java.util.concurrent.Callable;

public class Pesao implements Callable<Boolean> {

	Kane m_kane;
	String m_nombre;
	boolean saludado;

	public Pesao(Kane kane, String nombre) {
		m_kane = kane;
		m_nombre = nombre;
	}
		
	@Override
	public Boolean call() throws Exception {

		try {

			while (!saludado) {
			
				//System.out.println(m_nombre + " esta esperando");
	
				synchronized (m_kane) {
					m_kane.wait();
				}
								
				if (!m_kane.DarSaludo()) {
					//System.out.println(".");
					//return false;
				} else {
				
					Thread.sleep(500);
					System.out.println(m_nombre + " ha saludado");
					
					synchronized (m_kane.Saludado) {
						m_kane.Saludado.notify();
						saludado = true;
					}
					
					
				}
				
				
		
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return true;
	}

}
