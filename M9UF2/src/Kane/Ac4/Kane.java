package Kane.Ac4;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;

public class Kane implements Callable<Boolean>{

	String m_nombre;
	Random r = new Random();
	public Object Saludado = new Object();
	public int Saludos = 1;
	private boolean cansao = false;	
	
	public Kane(String nombre) {
		m_nombre = nombre;
	}
	
	public synchronized boolean DarSaludo() {
		
		if (Saludos > 0) {
			Saludos--;
			return true;
		}
		return false;
		
	}
	
	public synchronized void MuyCansao() {
		cansao = true;
	}
	
	@Override
	public Boolean call() {

		try {
			Thread.sleep(r.nextInt(50)+50);
			
			synchronized(this){
				this.notifyAll();	
			}
	
			while (true) {
										
				synchronized(Saludado) {
					Saludado.wait();
				
					synchronized (this) {
						if (cansao) {
							System.out.println();
							System.out.println("HAY ALGUIEN MUY PESADO YA");					
							return true;
						}
						Saludos++;
						this.notifyAll();
					}
					
				}
				
				
				
			}
			
			
			
		} catch (InterruptedException e) {
			System.out.println();
			System.out.println("YA ESTA BIEN DE SALUDAR HOMBRE");
		}
		
		return true;
	}

	
}
