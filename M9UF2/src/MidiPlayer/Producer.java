package MidiPlayer;

public class Producer implements Runnable {

	Note[] partitura;
	int tempo;
	int channel;
	
	public Producer(Note[] notes, int tempo, int channel) {
		partitura = notes;
		this.tempo = 60000/tempo/Note.Duration.negra;
		this.channel = channel;
	}
	
	@Override
	public void run() {
		try {
			System.out.println("Started Playing Music");
			for (int i = 0; i < partitura.length; i++) {
				MidiPlayer.play(channel, partitura[i]);
				Thread.sleep(this.tempo*partitura[i].getDuration());
				MidiPlayer.stop(channel, partitura[i]);	 
			}
		} catch (InterruptedException e) {

			e.printStackTrace();
			
		}
		
		System.out.println("Finished playing music");
	}

}
