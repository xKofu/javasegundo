package Bakery;

import java.util.concurrent.Callable;

public class Cliente implements Callable<Boolean> {

	private Tienda m_tienda;
	
	private boolean EnLaTienda = false;
	private boolean Cobrado = false;
	
	public Cliente(Tienda tienda) {
		m_tienda = tienda;
	}

	@Override
	public Boolean call() {
		
		try {
			while(!EnLaTienda) {
				if(!m_tienda.EntrarTienda()) {
					System.out.println(Thread.currentThread().getName() + " --> No me atienden manito");
					synchronized (m_tienda.SiguienteCliente) {
						m_tienda.SiguienteCliente.wait();
					}	
				}
				else {
					while(!Cobrado) {
						if(!m_tienda.CobrarCaja()) {
							synchronized (m_tienda.AtenderCaja) {
								m_tienda.AtenderCaja.wait();
							}
						}
						else {
							Thread.sleep(1000);
							m_tienda.SeMarchaCliente();
							
							synchronized (m_tienda.AtenderCaja) {
								m_tienda.AtenderCaja.notifyAll();
							}
							synchronized (m_tienda.SiguienteCliente) {
								m_tienda.SiguienteCliente.notifyAll();
							}
							
							System.out.println(Thread.currentThread().getName() +" --> Me han cobrado y salgo ya");

							Cobrado = true;
							EnLaTienda = true;
							
						}
						
					}
				}
			}
		}
		catch (InterruptedException e) {
			System.out.println("Bye me fui");
		}

		
		return true;
	}
	
}
