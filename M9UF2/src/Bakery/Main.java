package Bakery;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

	public static void main(String[] args) {
		ExecutorService executor = Executors.newCachedThreadPool();
		
		Tienda tienda = new Tienda();
		
		ArrayList<Future<Boolean>> futuros = new ArrayList<Future<Boolean>>();
		
		for (int i = 0; i < 20; i++) {
			futuros.add(executor.submit(new Cliente(tienda)));
		}
				
		
		for (Future<Boolean> futuro : futuros) {
			try {
				futuro.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println();
		System.out.println(" YA CERRAMOS QUE NO HAY NADIE MAS");
	}

}
