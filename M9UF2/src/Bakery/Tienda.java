package Bakery;

public class Tienda {

	Object SiguienteCliente = new Object();
	Object AtenderCaja = new Object();
	
	Object Sync1 = new Object();
	Object Sync2 = new Object();
	
	int capacidadTienda = 5;
	int capacidadCaja = 5;
	
	public boolean EntrarTienda() {
		synchronized (Sync1) {
			if (capacidadTienda > 0) {
				capacidadTienda--;
				return true;
			}
			return false;
		}
		
	}
	
	public boolean CobrarCaja() {
		synchronized (Sync2) {
			if (capacidadCaja > 0) {
				capacidadCaja--;
				return true;
			}
			return false;
		}
		
	}
	
	public void SeMarchaCliente() {
		capacidadTienda++;
		capacidadCaja++;
	}
	
}
