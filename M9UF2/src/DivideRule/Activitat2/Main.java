package DivideRule.Activitat2;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

	public static void main(String[] args) {

		Random r = new Random();
		float TiempoAlAcabarBucle = 0;
		float TiempoAlAcabarCalculo = 0;
		ArrayList<Integer> List = new ArrayList<Integer>();
		 
		System.out.println("ACTIVIDAD 2");
		
		for (int i = 0; i < 1000000; i++) {
			List.add(r.nextInt(1000000));
		}
		
		TiempoAlAcabarBucle = System.nanoTime();
		ExecutorService executor = Executors.newFixedThreadPool(1);
		Future<ArrayList<Integer>> resultat = executor.submit(new Coso(List.subList(0, List.size()/2)));
		Future<ArrayList<Integer>> resultat2 = executor.submit(new Coso(List.subList(List.size()/2, List.size())));
		executor.shutdown(); 
		
		try {
			System.out.println("Esperant que acabi el thread.");
			System.out.println();
			System.out.println(resultat.get()); 
			System.out.println(resultat2.get());
			TiempoAlAcabarCalculo = System.nanoTime();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} 
		
		System.out.println();
		System.out.println("Tiempo al empezar: " + TiempoAlAcabarBucle);
		System.out.println("Tiempo al acabar: " + TiempoAlAcabarCalculo);
		float f = TiempoAlAcabarCalculo - TiempoAlAcabarBucle;
		System.out.println();
		System.out.println("Nanosegundos transcurridos: " + f);
		
		
	}

}
