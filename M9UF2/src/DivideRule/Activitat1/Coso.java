package DivideRule.Activitat1;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class Coso implements Callable<ArrayList<Integer>> {

	ArrayList<Integer> m_List;
	ArrayList<Integer> res = new ArrayList<>();;

	int MaxNum = 0;
	int MinNum = 999999999;

	public Coso(ArrayList<Integer> list) {
		m_List = list;
	}

	public ArrayList<Integer> call() {

		for (int i = 0; i < m_List.size(); i++) {
			if (m_List.get(i) > MaxNum) {
				MaxNum = m_List.get(i); 
			}
			if (m_List.get(i) < MinNum) {
				MinNum = m_List.get(i);
			}
		}
		
		res.add(MaxNum);
		res.add(MinNum);
		
		return res;
		
	}

}
