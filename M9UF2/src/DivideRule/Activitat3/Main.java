package DivideRule.Activitat3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

	public static void main(String[] args) {

		Random r = new Random();
		float TiempoAlAcabarBucle = 0;
		float TiempoAlAcabarCalculo = 0;
		ArrayList<Integer> List = new ArrayList<Integer>();
		int Processors = Runtime.getRuntime().availableProcessors();

		System.out.println("ACTIVIDAD 3");

		for (int i = 0; i < 1000000; i++) {
			List.add(r.nextInt(1000000));
		}

		TiempoAlAcabarBucle = System.nanoTime();
		ExecutorService executor = Executors.newFixedThreadPool(1);
//		Future<ArrayList<Integer>> resultat = executor.submit(new Coso(List.subList(0, List.size()/2)));
//		Future<ArrayList<Integer>> resultat2 = executor.submit(new Coso(List.subList(List.size()/2, List.size())));

		int Repartir = List.size() / Processors;
		ArrayList<Future<ArrayList<Integer>>> AFut = new ArrayList<Future<ArrayList<Integer>>>();

		for (int i = 0; i < Processors; i++) {

			Future<ArrayList<Integer>> resultat = executor
					.submit(new Coso(List.subList(i * Repartir, (i + 1) * Repartir)));
			AFut.add(resultat);
		}
		executor.shutdown();

		int Mayor = 0;
		int Menor = 0;

		
		try {

			Mayor = AFut.get(0).get().get(0);
			Menor = AFut.get(0).get().get(1);

			for (int i = 1; i < AFut.size(); i++) {

				if (AFut.get(i).get().get(0) > Mayor) {
					Mayor = AFut.get(i).get().get(0);
				}
				if (AFut.get(i).get().get(1) < Menor) {
					Menor = AFut.get(i).get().get(1);
				}

			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TiempoAlAcabarCalculo = System.nanoTime();
		System.out.println("ACTIVIDAD 3");
		System.out.println();
		System.out.println("Numero mas grande -> " + Mayor);
		System.out.println("Numero mas peque�o -> " + Menor);

//		try {
//			System.out.println("Esperant que acabi el thread.");
//			System.out.println();
//			System.out.println(resultat.get()); 
//			System.out.println(resultat2.get());
//			TiempoAlAcabarCalculo = System.nanoTime();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		} catch (ExecutionException e) {
//			e.printStackTrace();
//		} 

		System.out.println();
		System.out.println("Tiempo al empezar: " + TiempoAlAcabarBucle);
		System.out.println("Tiempo al acabar: " + TiempoAlAcabarCalculo);
		float f = (TiempoAlAcabarCalculo - TiempoAlAcabarBucle) / 100000000f;
		System.out.println();
		System.out.println("Segundos transcurridos: " + f);

	}

}
