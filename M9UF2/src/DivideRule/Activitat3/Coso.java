package DivideRule.Activitat3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class Coso implements Callable<ArrayList<Integer>> {

	List<Integer> m_List;
	ArrayList<Integer> res = new ArrayList<>();;

	int MaxNum = 0;
	int MinNum = 999999999;

	public Coso(List<Integer> list) {
		m_List = list;
	}

	public ArrayList<Integer> call() {

		for (int i = 0; i < m_List.size(); i++) {
			if (m_List.get(i) > MaxNum) {
				MaxNum = m_List.get(i); 
			}
			if (m_List.get(i) < MinNum) {
				MinNum = m_List.get(i);
			}
		}
		
		res.add(MaxNum);
		res.add(MinNum);
		
		return res;
		
	}

}
