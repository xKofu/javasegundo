package examen;

import java.util.ArrayList;
import java.util.Collections;

public class AchievementManager {

	private ArrayList<Achievement> AchievementList = new ArrayList<Achievement>();
	
	public void RefreshAchievements() {
		
		/*
		for (int i = AchievementList.size()-1; i > 0; i--) {
			
			if (AchievementList.get(i).isFinished()) {
				RemoveAchievement(AchievementList.get(i));
			}
			
		}
		*/
		
		Collections.sort(AchievementList);
		
		System.out.println(AchievementList);
	}
	
	public void AddAchievement(Achievement a) {
		AchievementList.add(a);
	}
	
	public void RemoveAchievement(Achievement a) {
		AchievementList.remove(a);
	}
	
	
	
}
