package examen;

import java.util.ArrayList;

public abstract class Achievement implements Comparable<Achievement>, Observer{

	protected ArrayList<Reward> RewardList = new ArrayList<Reward>();
	protected String m_name;
	protected boolean Finished;
	
	public abstract void Process();

	public ArrayList<Reward> getRewardList() {
		return RewardList;
	}

	public void setRewardList(ArrayList<Reward> rewardList) {
		RewardList = rewardList;
	}

	public String getM_name() {
		return m_name;
	}

	public void setM_name(String m_name) {
		this.m_name = m_name;
	}

	public boolean isFinished() {
		return Finished;
	}

	public void setFinished(boolean finished) {
		Finished = finished;
	}

	@Override
	public int compareTo(Achievement a) {
		
		if (this.Finished && !a.isFinished()) {
			return -1;
		} else if (!this.Finished && a.isFinished()) {
			return 1;
		} else {
			if (this.m_name.compareTo(a.getM_name()) > 0) {
				return -1;
			} else if (this.m_name.compareTo(a.getM_name()) < 0) {
				return 1;
			} else {
				if (this.RewardList.size() > a.getRewardList().size()) {
					return -1;
				} else if (this.RewardList.size() < a.getRewardList().size()) {
					return 1;
				}
			}
		}
		
		return 0;
	}

	@Override
	public String toString() {
		return " {Achievement = " + m_name + ", Finished = " + Finished + "}";
	}
	
	
	
}
