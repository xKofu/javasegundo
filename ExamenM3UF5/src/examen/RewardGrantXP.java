package examen;

public class RewardGrantXP implements Reward {

	private int xp;
	
	public RewardGrantXP(int x) {
		
		xp = x;
		
	}
	
	@Override
	public void execute() {
		
		System.out.println("Gained " + xp + " experience points");
		
		System.out.println();
		
	}

}
