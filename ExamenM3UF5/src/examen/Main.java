package examen;

import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
		
			AchievementManager AM = new AchievementManager();
			EventManager EM = EventManager.getInstance();
			
			EM.AddEvent("OnCatch");
			EM.AddEvent("OnPick");
						
			AchievementCatch ACatch1 = new AchievementCatch("Your Journey Begins", 1);
			ACatch1.getRewardList().add(new RewardDrop("Pokeball"));
			AchievementCatch ACatch2 = new AchievementCatch("Beginner Trainer", 10);
			ACatch2.getRewardList().add(new RewardDrop("MasterBall"));
			ACatch2.getRewardList().add(new RewardGrantXP(1000));
			
			EM.registerObserver("OnCatch", ACatch1);
			EM.registerObserver("OnCatch", ACatch2);
			
			AM.AddAchievement(ACatch1);
			AM.AddAchievement(ACatch2);
			
			AchievementPick APick1 = new AchievementPick("My First Ball", "Pokeball");
			APick1.getRewardList().add(new RewardGrantXP(100));
			
			AchievementPick APick2 = new AchievementPick("Gotta catch �em all", "MasterBall");
			APick2.getRewardList().add(new RewardGrantXP(100));
			
			EM.registerObserver("OnPick", APick1);
			EM.registerObserver("OnPick", APick2);
			
			AM.AddAchievement(APick1);
			AM.AddAchievement(APick2);
			
			for (int i = 1; i < 11; i++) {
				AM.RefreshAchievements();
				System.out.println();
				System.out.println(" - Event Catch " + i + " - ");
				EM.notifyObservers("OnCatch", null);
				
			}
			
			AM.RefreshAchievements();
			
	}

}
