package examen;

public class AchievementPick extends Achievement {

	private String MyEvent = "OnPick";
	private String ExpectedObject;
	private String ReceivedObject;

	public AchievementPick(String name, String expectedObject) {
		super();
		ExpectedObject = expectedObject;
		m_name = name;
	}
	
	public String getExpectedObject() {
		return ExpectedObject;
	}

	public void setExpectedObject(String expectedObject) {
		ExpectedObject = expectedObject;
	}

	@Override
	public void notifyObserver(String event, Object o) {

		if (event.equals(MyEvent)) {

			ReceivedObject = (String) o;
			
			Process();

			if (Finished) {

				EventManager.getInstance().unregisterObserver(event, this);
				
				GiveReward();
				
			}

		}

	}

	private void GiveReward() {

		for (Reward reward : RewardList) {
			reward.execute();
		}

	}

	@Override
	public void Process() {
			
		if (ExpectedObject.equals(ReceivedObject)) {
			Finished = true;
		}

	}

}
