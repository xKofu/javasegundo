package examen;

import java.util.ArrayList;
import java.util.HashMap;

public class EventManager2 {
	
	private static EventManager2 em;
	
	HashMap<String, ArrayList<Achievement>> List;
	
	private EventManager() {
		List = new HashMap<String, ArrayList<Achievement>>();
	}
	
	public static EventManager getInstance() {
		
		if (em == null) {
			em = new EventManager();
		}
		
		return em;
	}
		
	public void AddEvent(String event) {
		List.put(event, new ArrayList<Achievement>());
	}
	
	public void registerObserver(String event, Achievement a) {

		List.get(event).add(a);
		
	}
	
	public void unregisterObserver(String event, Achievement a) {

		List.get(event).remove(a);
		
	}
		
	public void notifyObservers(String event, String Object) {
		
		for (int i = List.get(event).size()-1; i >= 0; i--) {
			
			List.get(event).get(i).notifyObserver(event, Object);
		
		}
	
	}

}

