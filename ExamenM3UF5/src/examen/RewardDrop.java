package examen;

public class RewardDrop implements Reward {

	private String Drop;
	
	public RewardDrop(String d) {
		
		Drop = d;
		
	}
	
	@Override
	public void execute() {
		
		System.out.println("You Recieved: " + Drop);
		System.out.println();
		EventManager.getInstance().notifyObservers("OnPick", Drop);
		
		
	}
	
	
}
