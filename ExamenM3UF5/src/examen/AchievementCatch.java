package examen;

public class AchievementCatch extends Achievement {

	private String MyEvent = "OnCatch";
	private int EventMaxCount;
	private int ActualCount = 0;

	public AchievementCatch(String name, int eventMaxCount) {
		super();
		EventMaxCount = eventMaxCount;
		m_name = name;
	}

	public int getEventMaxCount() {
		return EventMaxCount;
	}

	public void setEventMaxCount(int eventMaxCount) {
		EventMaxCount = eventMaxCount;
	}

	@Override
	public void notifyObserver(String event, Object o) {

		if (event.equals(MyEvent)) {

			Process();

			if (Finished) {

				EventManager.getInstance().unregisterObserver(event, this);
				
				GiveReward();

			}

		}

	}

	private void GiveReward() {

		for (Reward reward : RewardList) {
			reward.execute();
		}

	}

	@Override
	public void Process() {

		ActualCount++;
		
		if (ActualCount == EventMaxCount) {
			Finished = true;
		}

	}

}
