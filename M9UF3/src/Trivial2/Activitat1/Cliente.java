package Trivial2.Activitat1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import Trivial1.Actividad1.Result;
import Trivial1.Actividad1.StandartWrapper;

public class Cliente {

	public static void main(String[] args) {
		String hostName = "localhost";
		int portNumber = 60009;

		Manager Man = null;

		try (Socket echoSocket = new Socket(hostName, portNumber);
				BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))) {
			Man = new Manager(echoSocket);

			Man.receiveByte(Traductor.BYTE_WELCOME);
			Man.sendByte(Traductor.BYTE_ACK);
			
			System.out.println("Escoge un Nick");
			
			Man.send(stdIn.readLine());

			boolean flag = true;
			while (flag) {
				byte response = Man.receiveByte();
				switch (response) {
				case Traductor.BYTE_NICK_UNAVAILABLE:
					System.out.println("Nick Repetido, prueba otra vez!");
					Man.send(stdIn.readLine());
					break;
				case Traductor.BYTE_ACK:
					flag = false;
					break;
				default:
				}
			}

			boolean partida = true;

			while (partida) {
				
				flag = true;

				while (flag) {
					byte response = Man.receiveByte();
					switch (response) {
					case Traductor.BYTE_ON_GAME:
						System.out.println("Partida en Curso, espera a que acabe...");
						Man.sendByte(Traductor.BYTE_ACK);
						break;
					case Traductor.BYTE_QUEUE:
						System.out.println("A jugar, Espera a recibir la pregunta!");
						Man.sendByte(Traductor.BYTE_ACK);
						flag = false;
						break;
					default:
					}
					System.out.println();
				}

				LeerJson(Man.receive());
				Man.sendByte(Traductor.BYTE_ACK);
				Man.sendInt(Integer.parseInt(stdIn.readLine()));
				System.out.println("Esperando a recibir resultados...");
				Man.receiveByte(Traductor.BYTE_ACK);

				byte response = Man.receiveByte();
				switch (response) {
				case Traductor.BYTE_ANSWER_CORRECT:
					System.out.println("Respuesta Correcta!");
					Man.sendByte(Traductor.BYTE_ACK);
					break;
				case Traductor.BYTE_ANSWER_WRONG:
					System.out.println("Respuesta Incorrecta...");
					Man.sendByte(Traductor.BYTE_ACK);
					break;
				default:
				}
				System.out.println();

				LeerJson2(Man.receive());

				Man.sendByte(Traductor.BYTE_ACK);
				Man.receiveByte(Traductor.BYTE_CONTINUE);
				Man.sendByte(Traductor.BYTE_ACK);
				
				System.out.println("Quieres seguir jugando?");
				System.out.println("-si	-no");
				
				
				String Continuar = stdIn.readLine().toLowerCase();

				switch (Continuar) {
				case "s":
				case "si":
					Man.sendAndReceiveByte(Traductor.BYTE_CONTINUE_YES, Traductor.BYTE_ACK);
					break;
				case "n":
				case "no":
					Man.sendAndReceiveByte(Traductor.BYTE_CONTINUE_NO, Traductor.BYTE_ACK);
					partida = false;
					break;
				default:
				}
			}

			System.out.println("Gracias por jugar!");
			
		} catch (UnknownHostException e) {
			System.err.println("Don't know about host " + hostName);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection to " + hostName);
			System.exit(1);
		} catch (HeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				Man.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static void LeerJson(String json) {
		ObjectMapper mapper = new ObjectMapper();
		StandartWrapper Wrapper = null;

		try {
			Wrapper = mapper.readValue(json, StandartWrapper.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(Wrapper.getPregunta());
		System.out.println();
		int cont = 1;
		for (String string : Wrapper.getRespostes()) {
			System.out.println(cont + " - " + string);
			cont++;
		}
	}

	private static void LeerJson2(String json) {
		ObjectMapper mapper = new ObjectMapper();
		StandartWrapper Wrapper = null;

		try {
			Wrapper = mapper.readValue(json, StandartWrapper.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (Result r : Wrapper.getResultats()) {
			System.out.println(r.getNick() + " - " + r.getPuntuacio());
		}

	}

}
