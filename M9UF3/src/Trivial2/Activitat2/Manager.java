package Trivial2.Activitat2;

import java.io.*;
import java.net.*;

public class Manager {

	PrintWriter out;
	BufferedReader in;
	DataOutputStream outStream;
	DataInputStream inStream;
	
	public Manager(Socket echoSocket) throws IOException {
		out = new PrintWriter(echoSocket.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
		outStream = new DataOutputStream(echoSocket.getOutputStream());                   
    	inStream = new DataInputStream(echoSocket.getInputStream());

	}

	void send(String message) {

		out.println(message);

	}
	
	void sendByte(byte message) throws IOException {

		outStream.writeByte(message);

	}
	
	void sendInt(int message) throws IOException {

		outStream.writeInt(message);

	}

	String receive() throws IOException {

		return in.readLine();

	}
	
	byte receiveByte() throws IOException {

		return inStream.readByte();

	}
	
	int receiveInt() throws IOException {

		return inStream.readInt();

	}

	void receive(String message) throws IOException, HeyException {

		String hey = receive();
		if (!hey.equals(message)) {
			throw new HeyException("Esperaba " + message + " y recibi " + hey);
		}
	
	}
	
	void receiveByte(byte message) throws IOException, HeyException {

		byte hey = receiveByte();
		if (hey != message) {
			throw new HeyException("Esperaba " + message + " y recibi " + hey);
		}
	
	}

	String sendAndReceive(String message) throws IOException {

		send(message);
		return receive();

	}
	
	byte sendAndReceiveByte(byte message) throws IOException {

		sendByte(message);
		return receiveByte();

	}

	void sendAndReceive(String message, String response) throws IOException, HeyException {

		send(message);
		receive(response);

	}
	
	void sendAndReceiveByte(byte message, byte response) throws IOException, HeyException {

		sendByte(message);
		receiveByte(response);

	}

	void receiveAndSend(String message, String response) throws IOException, HeyException {

		receive(message);
		send(response);
		
	}
	
	void receiveAndSend(byte message, byte response) throws IOException, HeyException {

		receiveByte(message);
		sendByte(response);
		
	}

	void open(Socket socket) {

	}

	void close() throws IOException {
		in.close();
		out.close();
	}

}
