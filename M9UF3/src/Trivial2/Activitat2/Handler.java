package Trivial2.Activitat2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import Trivial1.Actividad1.StandartWrapper;

public class Handler implements Runnable {

	Socket socket;
	// PrintWriter out;
	// BufferedReader in;
	GeneralManager m_gm;
	Manager man;

	int Puntuacion;

	public Handler(Socket socket, GeneralManager gm) throws IOException {
		this.socket = socket;
		// out = new PrintWriter(socket.getOutputStream(), true);
		// in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		m_gm = gm;
		man = new Manager(socket);
	}

	public void run() {

		try {

			boolean Jugando = true;

			man.sendByte(Traductor.BYTE_WELCOME);
			man.receiveByte(Traductor.BYTE_ACK);
			String Nick = "";
			Puntuacion = 0;

			boolean NickIncorrecto = true;

			while (NickIncorrecto) {
				Nick = man.receive();
				if (m_gm.NickDisponible(Nick)) {
					NickIncorrecto = false;
					man.sendByte(Traductor.BYTE_ACK);
				} else {
					man.sendByte(Traductor.BYTE_NICK_UNAVAILABLE);
				}
			}

			while (Jugando) {
			
				boolean stop1 = true;
				
				while (stop1) {
					if (m_gm.SePuedeJugar()) {
						stop1 = false;
						synchronized (m_gm.Conectar) {
							m_gm.Conectar.notify();
						}
						man.sendByte(Traductor.BYTE_QUEUE);
					} else {
						man.sendByte(Traductor.BYTE_ON_GAME);
						synchronized (m_gm.Lobby) {
							m_gm.Lobby.wait();
						}
						man.receiveByte(Traductor.BYTE_ACK);
					}
				}

				man.receiveByte(Traductor.BYTE_ACK);

				synchronized (m_gm.ComenzarPartida) {
					m_gm.ComenzarPartida.wait();
				}

				man.send(PreguntaAleatoria()); // De momento solo hay 1

				man.receiveByte(Traductor.BYTE_ACK);
				int Respuesta = man.receiveInt();

				man.sendByte(Traductor.BYTE_ACK);

				m_gm.GenerarPuntuacion(Nick, Respuesta);

				
				synchronized (m_gm.Todos) {
					
					synchronized (m_gm.Jugado) {
						m_gm.Jugado.notify();
					}
					
					m_gm.Todos.wait();
				}

				if (ComprovarRespuesta(Respuesta)) {
					man.sendByte(Traductor.BYTE_ANSWER_CORRECT);
				} else {
					man.sendByte(Traductor.BYTE_ANSWER_WRONG);
				}
				
				man.receiveByte(Traductor.BYTE_ACK);
				
				String res = m_gm.DarResultados();

				man.send(res);

				man.receiveByte(Traductor.BYTE_ACK);

				man.sendByte(Traductor.BYTE_CONTINUE);

				man.receiveByte(Traductor.BYTE_ACK);

				switch (man.receiveByte()) {
				case Traductor.BYTE_CONTINUE_YES:
					break;
				case Traductor.BYTE_CONTINUE_NO:
					Jugando = false;
					break;
				}

				man.sendByte(Traductor.BYTE_ACK);

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			
		}

	}

	private boolean ComprovarRespuesta(int respuesta) {

		Random r = new Random();

		if (r.nextBoolean()) {
			Puntuacion++;
			return true;
		}

		return false;
	}

	private String PreguntaAleatoria() throws JsonProcessingException {

		StandartWrapper sw = new StandartWrapper();
		sw.setPregunta("El Kane es Maricon?");
		sw.getRespostes().add("Si");
		sw.getRespostes().add("Claramente");
		sw.getRespostes().add("Efectivamente");
		sw.getRespostes().add("Todas son correctas");

		ObjectMapper mapper = new ObjectMapper();

		String json = mapper.writeValueAsString(sw);

		return json;
	}

	public void Chuleta() {
//		* Bienvenido Cliente 
//		* --- Recibimos Ack
//		* --- Recibimos Nick
//		* Revisar Nick y enviar respuesta correspondiente -> Nick en uso / Ack
//		*		GM -------- Preguntamos por el nick recibido 
//		* ---Si esta en uso esperamos otro Nick (bucle)
//		* Enviamos estado de Lobby -> PartidaEnCurso / Dentro
//		*		GM -------- Preguntamos Estado de Partida
//		*		GM -------- Notificamos Conectar o Esperamos a Lobby dependiendo del estado de la partida
//		*		GM -------- Si nos conectamos, esperamos a que comience la partida
//		* --- (Si hay partida en curso recibimos ack y esperamos para volver a enviar) (sino recibimos Ack y continuamos)
//		* Enviamos Json con las preguntas (Generador de preguntas)
//		* --- Recibimos Ack
//		* --- Recibimos Int con la Respuesta
//		* Enviamos Ack
//		*		GM -------- Notificamos que hemos recibido respuesta
//		* Enviamos resultado de la pregunta (Correcta o Incorrecta)
//		* --- Recibimos Ack
//		*		GM -------- Esperamos a que todos los jugadores hayan acabado
//		* Enviamos Json con resultados de puntos totales de partida (Puntos de todos los jugadores)
//		* --- Recibimos Ack
//		* Enviamos Continuar?
//		* --- Recibimos Ack
//		* --- 2 Opciones -> Recibimos Seguir jugando (el bucle continua) / Recibimos Parar de jugar (Paramos bucle)
//		* Enviamos Ack
	}

}
