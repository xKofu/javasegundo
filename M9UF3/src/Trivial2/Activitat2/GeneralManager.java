package Trivial2.Activitat2;

import java.util.ArrayList;
import java.util.HashMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import Trivial1.Actividad1.Result;
import Trivial1.Actividad1.StandartWrapper;

public class GeneralManager implements Runnable {

	HashMap<String,Integer> Nicknames = new HashMap<String,Integer>();
	StandartWrapper Puntuaciones = new StandartWrapper();
	Object Sync1 = new Object();
	Object Sync2 = new Object();
	Object Sync3 = new Object();
	Object Sync4 = new Object();
	int nJugadores = 0;

	Object Lobby = new Object();
	Object Conectar = new Object();
	Object ComenzarPartida = new Object();

	Object Jugado = new Object();
	Object Todos = new Object();

	boolean PartidaEnCurso = false;

	public GeneralManager() {
	}

	@Override
	public void run() {

		try {

			while (true) {
				
				Nicknames.clear();
								
				PartidaEnCurso = false;
				
				synchronized (Lobby) {
					Lobby.notifyAll();
				}
				
				synchronized (Conectar) {
					Conectar.wait();
				}
				
				Puntuaciones = new StandartWrapper();

				Thread.sleep(5000);

				synchronized (ComenzarPartida) {
					ComenzarPartida.notifyAll();
				}
								
				PartidaEnCurso = true;

				EsperarJugadores();

				synchronized (Todos) {
					Todos.notifyAll();
				}
				
				

			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public boolean SePuedeJugar() {

		synchronized (Sync2) {
			if (PartidaEnCurso) {
				return false;
			}
			nJugadores++;
			return true;
		}

	}

	private void EsperarJugadores() {

		while (nJugadores > 0) {
			synchronized (Jugado) {
				try {
					Jugado.wait();
					nJugadores--;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	public boolean NickDisponible(String nick) {
		synchronized (Sync1) {
			if (!Nicknames.containsKey(nick)) {
				return false;
			}
			Nicknames.put(nick, 0);
			return true;
		}
	}

	public void GenerarPuntuacion(String nick, int Respuesta) {

//		synchronized (Sync3) {
//			Result r = new Result();
//			r.setNick(nick);
//			r.setPuntuacio(Puntos);
//			Puntuaciones.getResultats().add(r);
//		}

	}

	public String DarResultados() throws JsonProcessingException {

		synchronized (Sync4) {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(Puntuaciones);
		}

	}

}
