package Trivial2.Activitat2;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Servidor {

	 public static void main(String[] args)
	    {
	    	
	        int portNumber = 60009;
	        
	        try
	        (
	            ServerSocket serverSocket = new ServerSocket(portNumber);
	        ) {
	        	ExecutorService executors = Executors.newCachedThreadPool();
	        	
	        	GeneralManager gm = new GeneralManager();
	        	executors.execute(gm);
	        	
	        	while(true)
	        	{
	        		Socket clientSocket = serverSocket.accept();
	        		executors.execute(new Handler(clientSocket, gm));
	        		System.out.println("Se ha conectado 1 cliente");
	        	}
	        } catch (IOException e)
	        {
	            System.out.println("Exception caught when trying to listen to port "
	                + portNumber + " or listening for a connection");
	            System.out.println(e.getMessage());
	        }
	    }
	
}
