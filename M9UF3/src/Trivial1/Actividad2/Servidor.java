package Trivial1.Actividad2;

import java.net.*;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;

import Trivial1.Actividad1.StandartWrapper;

import java.io.*;

public class Servidor
{
    public static void main(String[] args)
    {
        
    	ArrayList<StandartWrapper> PreguntesList = new ArrayList<StandartWrapper>();
    	
        int portNumber = 60009;
        
        Manager man = null;
        
        try
        (
            ServerSocket serverSocket = new ServerSocket(portNumber);
            Socket clientSocket = serverSocket.accept();
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);                   
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        ) {
        	
        	man = new Manager(clientSocket);
        	
        	System.out.println("Connection established at port " + clientSocket.getPort()
        						+ " and local port " + clientSocket.getLocalPort());
            
        	man.sendAndReceive("BENVINGUT", "ACK");
        	
        	boolean activo = true;
        	
        	while(activo) {
        		man.sendAndReceive("PREPARAT", "ACK");
            	
            	StandartWrapper sw = new StandartWrapper();
            	sw.setPregunta("El Kane es Maricon?");
            	sw.getRespostes().add("Si");
            	sw.getRespostes().add("Claramente");
            	sw.getRespostes().add("Efectivamente");
            	sw.getRespostes().add("Todas son correctas");
            	
        		ObjectMapper mapper = new ObjectMapper();
            	
            	String json = mapper.writeValueAsString(sw);
            	
            	man.sendAndReceive(json, "ACK");
            	String response = man.receive();
            	man.send("ACK");
            	if (response.equals("4")) {
            		man.send(":pogchamp:");
            	} else {
            		man.send(":peepoclown:");
            	}
            	
            	man.receiveAndASend("ACK", "'TAS PICAO?");
            	
            	String response2 = man.receive();
            	
            	switch(response2) {
	            	case "NIPAH":
	            		// gucci
	            		break;
	            	case "PLEGA":
	            		activo = false;
	            		man.send("ACK");
	            		break;
	            	default:
	            		System.out.println("Eso no es una respuesta correcta, que eres tontisimo");
            	}
        	}
        	
        	
        } catch (IOException | HeyException e)
        {
            System.out.println("Exception caught when trying to listen to port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
}

