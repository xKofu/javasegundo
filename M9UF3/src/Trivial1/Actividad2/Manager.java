package Trivial1.Actividad2;

import java.io.*;
import java.net.*;

public class Manager {

	PrintWriter out;
	BufferedReader in;

	public Manager(Socket echoSocket) throws IOException {
		out = new PrintWriter(echoSocket.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
	}

	void send(String message) {

		out.println(message);

	}

	String receive() throws IOException {

		return in.readLine();

	}

	void receive(String message) throws IOException, HeyException {

		String hey = receive();
		if (!hey.equals(message)) {
			throw new HeyException("Esperaba " + message + " y recibi " + hey);
		}
	}

	String sendAndReceive(String message) throws IOException {

		send(message);
		return receive();

	}

	void sendAndReceive(String message, String response) throws IOException, HeyException {

		send(message);
		receive(response);

	}

	void receiveAndASend(String message, String response) throws IOException, HeyException {

		receive(message);
		send(response);
		
	}

	void open(Socket socket) {

	}

	void close() throws IOException {
		in.close();
		out.close();
	}

}
