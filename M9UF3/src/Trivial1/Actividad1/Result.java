package Trivial1.Actividad1;

public class Result {

	String nick;
	int puntuacio;

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public int getPuntuacio() {
		return puntuacio;
	}

	public void setPuntuacio(int puntuacio) {
		this.puntuacio = puntuacio;
	}

	public Result() {
		super();
	}

	public Result(String nick, int puntuacio) {
		super();
		this.nick = nick;
		this.puntuacio = puntuacio;
	}

	@Override
	public String toString() {
		return "Result [nick=" + nick + ", puntuacio=" + puntuacio + "]";
	}

}
