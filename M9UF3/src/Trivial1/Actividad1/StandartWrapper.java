package Trivial1.Actividad1;

import java.util.ArrayList;

public class StandartWrapper {

	String pregunta;

	ArrayList<String> respostes;

	ArrayList<Result> resultats;

	public StandartWrapper() {
		super();
		respostes = new ArrayList<String>();
		resultats = new ArrayList<Result>();
	}

	public StandartWrapper(String pregunta, ArrayList<String> respostes, ArrayList<Result> resultats) {
		super();
		this.pregunta = pregunta;
		this.respostes = respostes;
		this.resultats = resultats;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public ArrayList<String> getRespostes() {
		return respostes;
	}

	public void setRespostes(ArrayList<String> respostes) {
		this.respostes = respostes;
	}

	public ArrayList<Result> getResultats() {
		return resultats;
	}

	public void setResultats(ArrayList<Result> resultats) {
		this.resultats = resultats;
	}

	@Override
	public String toString() {
		return "StandartWrapper [pregunta=" + pregunta + ", respostes=" + respostes + ", resultats=" + resultats + "]";
	}

}
