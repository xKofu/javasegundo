package Trivial1.Actividad1;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Cliente
{
    public static void main(String[] args)
    {
        /*
        if (args.length != 2)
        {
            System.err.println("Expected HOSTNAME and PORT as parameter");
            System.exit(1);
        }

        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);
        */
//        String hostName = "10.1.82.10";
    	String hostName = "localhost";
        int portNumber = 60009;

        Manager Man = null;
        try
        (
            Socket echoSocket = new Socket(hostName, portNumber);
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))
        ) {
        	Man = new Manager(echoSocket);
        	
        	Man.receiveAndASend("BENVINGUT", "ACK");
        	
        	boolean flag = true;
        	
        	while(flag) {
        		
        		Man.receiveAndASend("PREPARAT", "ACK");
            	String json = Man.receive();
            	
            	LeerJson(json);
            	
            	Man.send("ACK");
            	Man.send(stdIn.readLine());
            	Man.receive("ACK");
            	
            	String response = Man.receive();
            	
            	switch(response) {
            	
            	case ":pogchamp:":
            		System.out.println("Good Job!");
            		break;
            	case ":peepoclown:":
            		Cachondeo.main(args);
            		break;
            	default:
            		throw new HeyException("Esperaba pogchamp o peepoclown y me ha venido otra vaina");
            	}
            	
            	Man.send("ACK");
            	
            	System.out.println("╔�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?╗");
        		System.out.println("║  Quieres Seguir Jugando?║");
        		System.out.println("║    - Si         - No    ║");
        		System.out.println("╚�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?�?");

        		boolean respuestaCorrecta = false;
        			        		
        		while (!respuestaCorrecta) {
        			String res = stdIn.readLine();
        		
        			switch (res) {
					case "Si":
					case "S":
					case "si":
					case "s":
					case "SI":
						Man.receiveAndASend("'TAS PICAO?", "NIPAH");
						respuestaCorrecta = true;
						break;
					case "No":
					case "N":
					case "no":
					case "n":
					case "NO":
						flag = false;
						Man.receiveAndASend("'TAS PICAO?", "PLEGA");
						respuestaCorrecta = true;
						break;
					default:
						System.out.println(res + " no es una respuesta valida, vuelve a probar");
						System.out.println();
						break;
					}
        		}
        		
        		
        		
        	}
        		
        	
        } catch (UnknownHostException e)
        {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e)
        {
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            System.exit(1);
        } catch (HeyException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			try {
				Man.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }


	private static void LeerJson(String json) {
		ObjectMapper mapper = new ObjectMapper();
 		StandartWrapper Wrapper = null;
         
         try {
 			Wrapper = mapper.readValue(json, StandartWrapper.class);
 		} catch (JsonParseException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		} catch (JsonMappingException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		} catch (IOException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}


 		System.out.println(Wrapper.getPregunta());
 		System.out.println();
 		int cont = 1;
 		for (String string : Wrapper.getRespostes()) {
 			System.out.println(cont + " - " + string);
 			cont++;
 		}
	}
}
