package Trivial1.Actividad1;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class otroMainJsonToData {

	public static void main(String[] args) {
	
		String JsonString = "{\r\n" + 
				" \"respostes\": [\r\n" + 
				"	\"Resposta1\",\r\n" + 
				"	\"Resposta2\",\r\n" + 
				"	\"Resposta3\"\r\n" + 
				"	],\r\n" + 
				" \"pregunta\" : \"Enunciado de la pregunta\"\r\n" + 
				"}";
		
		String JsonString2 = "{\r\n" + 
				" \"respostes\": [\r\n" + 
				"	\"Resposta1\",\r\n" + 
				"	\"Resposta2\"\r\n" + 
				"	],\r\n" + 
				" \"pregunta\" : \"Enunciado de la pregunta 2\"\r\n" + 
				"}";
				
		String JsonString3 = "{\r\n" + 
				" \"respostes\": [\r\n" + 
				"	\"Resposta1\",\r\n" + 
				"	\"Resposta2\",\r\n" + 
				"	\"Resposta3\",\r\n" + 
				"	\"Resposta4\",\r\n" + 
				"	\"Resposta5\",\r\n" + 
				"	\"Resposta6\",\r\n" + 
				"	\"Resposta7\",\r\n" + 
				"	\"Resposta8\"\r\n" + 
				"	],\r\n" + 
				" \"pregunta\" : \"Enunciado de la pregunta 3\"\r\n" + 
				"}";		
		//System.out.println(JsonString);
		
		ObjectMapper mapper = new ObjectMapper();
		
		StandartWrapper Wrapper = null;
		

		try {
			Wrapper = mapper.readValue(JsonString3, StandartWrapper.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		System.out.println(Wrapper.getPregunta());
		System.out.println();
		int cont = 1;
		for (String string : Wrapper.getRespostes()) {
			System.out.println(cont + " - " + string);
			cont++;
		}
		
	}

}
