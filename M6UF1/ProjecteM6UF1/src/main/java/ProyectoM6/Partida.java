package ProyectoM6;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="joc")
public class Partida {
	
	private ArrayList<Jugador> Jugadores;

	public Partida() {
		Jugadores = new ArrayList<Jugador>();
	}
	
	@XmlElementWrapper(name="jugadores")
	@XmlElement(name="jugador")
	public ArrayList<Jugador> getJugadores() {
		return Jugadores;
	}

	public void setJugadores(ArrayList<Jugador> jugadores) {
		Jugadores = jugadores;
	}

	@Override
	public String toString() {
		return "Partida [Jugadores=" + Jugadores + "]";
	}
		
	
}
