package ProyectoM6;

import javax.xml.bind.annotation.XmlElement;

public class Configuracion {

	private boolean modo_daltonico;
	private  String tipo_controlador;
	private String resolucion_pantalla;
	
	
	
	public Configuracion() {
		super();
	}
	
	public Configuracion(boolean modo_daltonico, String tipo_controlador, String resolucion_pantalla) {
		this.modo_daltonico = modo_daltonico;
		this.tipo_controlador = tipo_controlador;
		this.resolucion_pantalla = resolucion_pantalla;
	}
	
	@XmlElement
	public boolean getModo_daltonico() {
		return this.modo_daltonico;
	}
	
	public void setModo_daltonico(boolean dal) {
		this.modo_daltonico = dal;
	}
	
	
	@XmlElement
	public String getTipo_controlador() {
		return this.tipo_controlador;
	}
	
	public void setTipo_controlador(String Controles) {
		this.tipo_controlador = Controles;
	}
	
	
	
	@XmlElement
	public String getResolucion_pantalla() {
		return this.resolucion_pantalla;
	}
	
	public void setResolucion_pantalla(String pantalla) {
		this.resolucion_pantalla = pantalla;
	}
	
	
	public Configuracion getConfiguracion() {
		return this;
	
	}
	
	
	
	@Override
	public String toString() {
		return "Configuracion [ Modo Daltonico = " + this.modo_daltonico + ", Tipo Controlador = " + this.tipo_controlador + ", Resulocion Pantalla = " + this.resolucion_pantalla + "]";
	}
	
	
	
	
	
	
}
