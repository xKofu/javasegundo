package ProyectoM6;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;







public class XMLWriter{

	
	public static void main(String[] args) throws JAXBException {
		File f = new File("Jugador.xml");
		
		JAXBContext context = JAXBContext.newInstance(Partida.class);
		//el reader se llama unmarshaller
		Unmarshaller um = context.createUnmarshaller();
		Jugador Kane = (Jugador) um.unmarshal(f);
		

		Kane.setNombre("Ruben");
		Kane.setIniciales("RUB");
		Kane.setPosicion(1);
		Kane.confPlayer.setModo_daltonico(true);
		Kane.confPlayer.setTipo_controlador("volante");
		Kane.confPlayer.setResolucion_pantalla("1920x1024");
		
	
		
	
		Marshaller m = context.createMarshaller();
		m.marshal(Kane, f);

		
		
	}
	
	

	public static void canviarInicials(String nomFitxer, String nomJugador, String novesInicials) throws JAXBException {

		Partida p = LeerXML(nomFitxer);

		for (Jugador j : p.getJugadores()) {
			if (j.getNombre().equals(nomJugador)) {
				j.setIniciales(novesInicials);

				EscribirXML(p, nomFitxer);

				return;
			}
		}
	}
	
	

	public static void toggleDaltonic(String nomFitxer, String nomJugador) throws JAXBException {

        Partida p = LeerXML(nomFitxer);

        for (Jugador j : p.getJugadores()) {
            if (j.getNombre().equals(nomJugador)) {
                Configuracion conf = j.getConfPlayer();
                conf.setModo_daltonico(!conf.getModo_daltonico());

                EscribirXML(p, nomFitxer);

                return;
            }
        }

    }
	
	
	
	
	/* 
	 afegeixJugador(String nomFitxer, Jugador jug) - afegeix un jugador al fitxer xml. 
	 A més, aquest jugador té la posició 0 amb 0 punts. Sobreescriu el fitxer xml(2 Punts)
	*/
	
	public static void afegeixJugador(String nomFitxer, Jugador jug) throws JAXBException {
		

		Partida p = LeerXML(nomFitxer);

		jug.setPosicion(0);
		ArrayList<Integer> b = new ArrayList<Integer>();
		b.add(0);
		jug.setPuntuaciones(b);
		ArrayList<Jugador> a = p.getJugadores();
		a.add(jug);

		EscribirXML(p, nomFitxer);
	}
	
	/* mostrarClassificacio(String nomFitxer) – mostra la classificació del joc. 
	  En aquesta classificació es mostra nom, cognoms i punts dels jugadors. (2 Punts)
	*/
	
	public static void mostrarClassificacio(String nomFitxer) throws JAXBException {
		Partida p = LeerXML(nomFitxer);
		for(Jugador j : p.getJugadores()) 
			System.out.println(j.getNombre() + " " + j.getApellido() + " " + j.getPuntuaciones());                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
	}
	
	/* actualitzarClassificacio(Joc joc) - actualitza la classificació del joc. 
	 Per tots els jugadors, mira de la seva llista de puntuacions la més alta, 
	 i actualitza el camp de posicions fent que el que té la puntuació més alta tingui la posició 1, 
	 el segon la posició 2, etc. Torna l’objecte de classe joc, no escriu en el fitxer xml. (2 punts)
	*/
	
	public static Partida actualitzarClassificacio(Partida joc) throws JAXBException {
		
		ArrayList<Integer> PuntuacionAlta = new ArrayList<Integer>();
		
		for(Jugador j : joc.getJugadores()) {
			ArrayList<Integer> PuntosJ = j.getPuntuaciones();
			Collections.sort(PuntosJ);
			PuntuacionAlta.add(PuntosJ.get(PuntosJ.size()-1));
		} 
		Collections.sort(PuntuacionAlta);
		Collections.reverse(PuntuacionAlta);
		
		
		
		return joc;
	}
	
	
	/* 
	 	alCarrer(String nomFitxer, String nomJugador) - elimina un jugador del fitxer xml. A més, 
	 
	  	s’ha d’actualitzar les posicions de la resta de jugadors. Sobreescriu el fitxer xml (1 Punts)
	 
	 */
	
	public static void alCarrer(String nomFitxer, String nomJugador) throws JAXBException {
		
		Partida p = LeerXML(nomFitxer);

		ArrayList<Jugador> a = p.getJugadores();

		int contador = 0;
		for (int i = 0; i < a.size(); i++) {
			if (a.get(i).getNombre().equals(nomJugador)) {
				a.remove(i);
				return;
			}
			contador++;
		}
		for (; contador < a.size(); contador++) {
			a.get(contador).setPosicion(a.get(contador).getPosicion() - 1);
		}

		p.setJugadores(a);

		EscribirXML(p, nomFitxer);
	}
	
	/* afegeixPuntuacio(String nomFitxer, String nomJugador, int punt) - 
	   afegeix una puntuacio del jugador amb el nom proporcionat. 
	   A més, s’ha d’actualitzar les posicions tots els jugadors. Sobreescriu el fitxer xml  (1 Punts)
	*/
	 
	
	
	public static void afegeixPuntuacio(String nomFitxer, String nomJugador, int punt) throws JAXBException {

		Partida p = LeerXML(nomFitxer);
		
		
		ArrayList<Jugador> a = p.getJugadores();
		
		for (Jugador jugador : p.getJugadores()) {
			if(jugador.getNombre().equals(nomJugador)) {
				ArrayList<Integer> b = jugador.getPuntuaciones();
				
				b.add(punt);
				
				jugador.setPuntuaciones(b);
				
				Collections.sort(b);
				Collections.reverse(b);
			}
			
		}
		
		Collections.sort(a);
		
		int posicion = 1;
		for (Jugador jugador : p.getJugadores()) {
		
			jugador.setPosicion(posicion);
			posicion++;
		}
		
		EscribirXML(p, nomFitxer);

	}
	
	
	
	

	public static Partida LeerXML(String nomFitxer) throws JAXBException {

		File f = new File(nomFitxer);

		JAXBContext context = JAXBContext.newInstance(Partida.class);
		Unmarshaller um = context.createUnmarshaller();

		Partida p = (Partida) um.unmarshal(f);

		return p;

	}

	public static void EscribirXML(Partida p, String nomFitxer) throws JAXBException {

		File f = new File(nomFitxer);

		JAXBContext context = JAXBContext.newInstance(Partida.class);

		Marshaller m = context.createMarshaller();

		m.marshal(p, f);

	}


}