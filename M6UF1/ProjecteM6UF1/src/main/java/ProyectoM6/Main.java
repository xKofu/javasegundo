package ProyectoM6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Main {

	public static void main(String[] args) throws JAXBException, FileNotFoundException, IOException, ParseException {
		
		// XML //
		
		// Canviar Iniciales
		
		//canviarInicials("Partida.xml", "Kane", "RUB");
				
		// Canviar Daltonico
		
		//toggleDaltonic("Partida.xml", "Kane");		
				
		// A�adir Jugador
		/*
		Jugador NewPlayer = new Jugador("Eric", "ENM", "8762398M");
		afegeixJugador("Partida.xml", NewPlayer);
		*/

		// Mostrar Clasi

		//mostrarClassificacio("Partida.xml");

		// Actualizar Clasi
		/*
		Partida p = LeerXML("Partida.xml");
		p = actualitzarClassificacio(p);
		EscribirXML(p, "Partida.xml");
		*/
		
		// A la calle
		
		//alCarrer("Partida.xml", "Eric");

		// A�adir Puntuacion
		
		//afegeixPuntuacio("Partida.xml", "Kane", 1234);
		
		// JSON //
		
		// Listar enemigos
		
		//LlistarEnemics("enemics.json");
		
		// A�adir enemigos

		//afegirEnemic("enemics.json", "Manolo", false, 8);
		
		// A�adir Kill
		
		//afegirKill("enemics.json", "Goomba", 2);
		
		// Calcular Puntuaci�n

		//calcularPuntuacio("enemics.json");
		
		// A�adir puntuacion en base a partida
		
		//AfegirPuntuacioEnBaseAPartida("Partida.xml", "Kane", "enemics.json");
		
	}

	// XML

	public static Partida LeerXML(String nomFitxer) throws JAXBException {

		File f = new File(nomFitxer);

		JAXBContext context = JAXBContext.newInstance(Partida.class);
		Unmarshaller um = context.createUnmarshaller();

		Partida p = (Partida) um.unmarshal(f);

		return p;

	}

	public static void EscribirXML(Partida p, String nomFitxer) throws JAXBException {

		File f = new File(nomFitxer);

		JAXBContext context = JAXBContext.newInstance(Partida.class);

		Marshaller m = context.createMarshaller();

		m.marshal(p, f);

	}

	public static void canviarInicials(String nomFitxer, String nomJugador, String novesInicials) throws JAXBException {

		Partida p = LeerXML(nomFitxer);

		for (Jugador j : p.getJugadores()) {
			if (j.getNombre().equals(nomJugador)) {
				j.setIniciales(novesInicials);

				EscribirXML(p, nomFitxer);

				return;
			}
		}
	}

	public static void toggleDaltonic(String nomFitxer, String nomJugador) throws JAXBException {

		Partida p = LeerXML(nomFitxer);

		for (Jugador j : p.getJugadores()) {
			if (j.getNombre().equals(nomJugador)) {
				Configuracion conf = j.getConfPlayer();
				conf.setModo_daltonico(!conf.getModo_daltonico());

				EscribirXML(p, nomFitxer);

				return;
			}
		}

	}

	public static void afegeixJugador(String nomFitxer, Jugador jug) throws JAXBException {

		Partida p = LeerXML(nomFitxer);

		jug.setPosicion(0);
		ArrayList<Integer> b = new ArrayList<Integer>();
		b.add(0);
		jug.setPuntuaciones(b);
		ArrayList<Jugador> a = p.getJugadores();
		a.add(jug);

		EscribirXML(p, nomFitxer);
	}

	public static void mostrarClassificacio(String nomFitxer) throws JAXBException {
		Partida p = LeerXML(nomFitxer);
		for (Jugador j : p.getJugadores())
			System.out.println(j.getNombre() + " " + j.getApellido() + " " + j.getPuntuaciones());
	}

	public static Partida actualitzarClassificacio(Partida joc) throws JAXBException {

		for (Jugador j : joc.getJugadores()) {
			
			ArrayList<Integer> PuntosJ = j.getPuntuaciones();
			
			Collections.sort(PuntosJ);
			Collections.reverse(PuntosJ);
			
			j.setPuntuaciones(PuntosJ);
					
		}
		ArrayList<Jugador> Aux = joc.getJugadores();

		Collections.sort(Aux);
		Collections.reverse(Aux);
		
		int k = 1;
			
		for (Jugador j : Aux) {
			
			j.setPosicion(k);
			k++;
					
		}
		
		joc.setJugadores(Aux);
		
		return joc;
	}

	public static void alCarrer(String nomFitxer, String nomJugador) throws JAXBException {
		
		Partida p = LeerXML(nomFitxer);

		ArrayList<Jugador> a = p.getJugadores();
		
		boolean flag = false;
		
		for (int i = 0; i < a.size(); i++) {
			
			if (a.get(i).getNombre().equals(nomJugador)) {
				
				a.remove(i);	
				flag = true;
			}
			
		}
		
		if (flag) {
			
			p = actualitzarClassificacio(p);
			
		}
		

		EscribirXML(p, nomFitxer);
		
	}

	public static void afegeixPuntuacio(String nomFitxer, String nomJugador, int punt) throws JAXBException {

		Partida p = LeerXML(nomFitxer);

		ArrayList<Jugador> a = p.getJugadores();

		for (Jugador jugador : p.getJugadores()) {
			if (jugador.getNombre().equals(nomJugador)) {
				ArrayList<Integer> b = jugador.getPuntuaciones();

				b.add(punt);

				jugador.setPuntuaciones(b);

			}

		}

		p = actualitzarClassificacio(p);

		EscribirXML(p, nomFitxer);

	}

	// JSON

	public static void LlistarEnemics(String nomFitxer) throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		// pillo el json
		JSONArray ListaJson = (JSONArray) parser.parse(new FileReader(nomFitxer));
		
		for(Object eneimgo : ListaJson) {
			JSONObject m_enemigo = (JSONObject) eneimgo;
			System.out.println(m_enemigo.get("nom") + " " + m_enemigo.get("puntsBase") + " " + m_enemigo.get("numMorts"));
		}
	}
	
	public static void afegirEnemic(String nomFitxer, String nomEnemic, boolean elite, int puntsBase) throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		// pillo el json
		JSONArray ListaJson = (JSONArray) parser.parse(new FileReader(nomFitxer));
		
		JSONObject new_enemigo = new JSONObject();
		
		new_enemigo.put("nom", nomEnemic);
		new_enemigo.put("elite", elite);
		new_enemigo.put("puntsBase", puntsBase);
		
		new_enemigo.put("numMorts", 0);
		new_enemigo.put("bonus", new ArrayList<Long>()); 
		
		
		ListaJson.add(new_enemigo);
		

		try (FileWriter file = new FileWriter(nomFitxer)) {

			file.write(ListaJson.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public static void afegirKill(String nomFitxer, String nomEnemic, double bonus) throws FileNotFoundException, IOException, ParseException{
		JSONParser parser = new JSONParser();
		
		JSONArray ListaJson = (JSONArray) parser.parse(new FileReader(nomFitxer));
		
		for (Object object : ListaJson) { 
			JSONObject m_enemic = (JSONObject) object;
			
			if(m_enemic.get("nom").equals(nomEnemic)) {
				m_enemic.put("numMorts", (Long)m_enemic.get("numMorts")+1);
				
				ArrayList<Double> a = (ArrayList<Double>) m_enemic.get("bonus");
				a.add(bonus);
				m_enemic.put("bonus", a);
				
				
				try (FileWriter file = new FileWriter(nomFitxer)) {

					file.write(ListaJson.toJSONString());
					file.flush();

				} catch (IOException e) {
					e.printStackTrace();
				}
				
				return;
			}
			
		}
		
	}

	public static int calcularPuntuacio (String nomFitxer) throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		
		JSONArray ListaJson = (JSONArray) parser.parse(new FileReader(nomFitxer));
		
		
		
		double acumuladorFinal = 0.0;
	
		for(Object a : ListaJson) {
			JSONObject m_enemic = (JSONObject) a;
			
			JSONArray m_ListaBonusE = (JSONArray) m_enemic.get("bonus");
			Long  mult = (Long) m_enemic.get("puntsBase");
			
			double acumulador = 0.0;
			for (int i = 0; i < m_ListaBonusE.size(); i++) {
				
				if(m_ListaBonusE.get(i) instanceof Long) {
					Long asd = (Long)m_ListaBonusE.get(i);
					acumulador += asd * mult;
				}
				else {
					double asd = (double) m_ListaBonusE.get(i);
					acumulador += asd * mult;
				}
			}
			boolean elite = (boolean)m_enemic.get("elite");
			if(elite) {
				acumulador = acumulador*2;
			}

			acumuladorFinal += acumulador;
		}
		
		
		
		return (int)acumuladorFinal;
		
	}
	
	public static void AfegirPuntuacioEnBaseAPartida(String nomFitxerXML, String nomJugador, String nomFitxerJSON) throws JAXBException, FileNotFoundException, IOException, ParseException{
		
		Partida p = LeerXML(nomFitxerXML);
			
		JSONParser parser = new JSONParser();
		JSONArray ListaJson = (JSONArray) parser.parse(new FileReader(nomFitxerJSON));
				
		int total = calcularPuntuacio(nomFitxerJSON);
		
		afegeixPuntuacio(nomFitxerXML, nomJugador, total);
		
		
	}
	
}
