package ProyectoM6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONWriter {
	
	public static void main(String[] args) throws ParseException, FileNotFoundException, IOException, JAXBException {
		
		
		JSONParser parser = new JSONParser();
		// pillo el json
		JSONArray todo = (JSONArray) parser.parse(new FileReader("enemigos.json"));
		// return lista java
		
		
		/*
		JSONObject elPrimerGoomba = (JSONObject) todo.get(0);
		System.out.println(elPrimerGoomba);
		System.out.println(elPrimerGoomba.get("nom"));
		
		
		
		for (Object object : todo) { // por todos los objetos de la lista
			JSONObject goomba = (JSONObject) object;
			goomba.put("numMorts", (Long)goomba.get("numMorts")+1);
			goomba.put("caneMarica",true);
		}
		System.out.println(todo);
		*/
	
		
		
		
		// escribo en el json
		try (FileWriter file = new FileWriter("enemigos2.json")) {

			file.write(todo.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		System.out.println();
		System.out.println();
		System.out.println();
		
		
		// ej 1 JSON funciona
		LlistarEnemics("enemigos.json");
		
		System.out.println();
		System.out.println();
		System.out.println();
		
		
		
		//ej 2 JSON funciona
		
		afegirEnemic("enemigos.json", "EnemigoTest01", false, 10);
		
		System.out.println();
		System.out.println();
		System.out.println();
		
		for (int i = 0; i < todo.size(); i++) {
			
			System.out.println(todo.get(i));
		}
		
		
		//ej 3 JSON funciona
		afegirKill("enemigos.json", "Goomba", 2);
		System.out.println();
		System.out.println();
		System.out.println();
		
		System.out.println(todo.get(0));
		
		
		//ej 4 JSON funciona 
		System.out.println();
		System.out.println();
		System.out.println();
		
		System.out.println(calcularPuntuacio("enemigos.json"));
		
		
	}
	
	/*
	 LlistarEnemics(String nomFitxer) – Mostra un llistat dels enemics. Per cada enemic et diu el seu nom, punts base, i número de morts (1 punt)
	*/
	
	//funciona
	public static void LlistarEnemics(String nomFitxer) throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		// pillo el json
		JSONArray ListaJson = (JSONArray) parser.parse(new FileReader(nomFitxer));
		
		for(Object eneimgo : ListaJson) {
			JSONObject m_enemigo = (JSONObject) eneimgo;
			System.out.println(m_enemigo.get("nom") + " " + m_enemigo.get("puntsBase") + " " + m_enemigo.get("numMorts"));
		}
	}
	
	/*
	 
	 afegirEnemic(String nomFitxer, String nomEnemic, boolean elite, int puntsBase) – 
	 Afegeix un nou enemic al fitxer JSON amb les dades proporcionades, 0 numMorts, 
	 i una llista buida com a bonus. Sobreescriu el fitxer JSON (2 punts)
	
	 */
	
	//funciona
	public static void afegirEnemic(String nomFitxer, String nomEnemic, boolean elite, int puntsBase) throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		// pillo el json
		JSONArray ListaJson = (JSONArray) parser.parse(new FileReader(nomFitxer));
		
		JSONObject new_enemigo = new JSONObject();
		
		new_enemigo.put("nom", nomEnemic);
		new_enemigo.put("elite", elite);
		new_enemigo.put("puntsBase", puntsBase);
		
		new_enemigo.put("numMorts", 0);
		new_enemigo.put("bonus", new ArrayList<Long>()); 
		
		
		ListaJson.add(new_enemigo);
		

		try (FileWriter file = new FileWriter(nomFitxer)) {

			file.write(ListaJson.toJSONString());
			file.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/* 
	 * 
	afegirKill(String nomFitxer, String nomEnemic, double bonus) – 
	S’afegeix una nova kill al fitxer. Incrementa en 1 el nombre de morts, 
	i afegeix el bonus al final de la llista de bonus. Sobreescriu el fitxer JSON (2 Punts)
	
	*/
	
	
	//funciona
	public static void afegirKill(String nomFitxer, String nomEnemic, double bonus) throws FileNotFoundException, IOException, ParseException{
		JSONParser parser = new JSONParser();
		
		JSONArray ListaJson = (JSONArray) parser.parse(new FileReader(nomFitxer));
		
		for (Object object : ListaJson) { 
			JSONObject m_enemic = (JSONObject) object;
			
			if(m_enemic.get("nom").equals(nomEnemic)) {
				m_enemic.put("numMorts", (Long)m_enemic.get("numMorts")+1);
				
				ArrayList<Double> a = (ArrayList<Double>) m_enemic.get("bonus");
				a.add(bonus);
				m_enemic.put("bonus", a);
				
				
				try (FileWriter file = new FileWriter(nomFitxer)) {

					file.write(ListaJson.toJSONString());
					file.flush();

				} catch (IOException e) {
					e.printStackTrace();
				}
				
				return;
			}
			
		}
		
	}
	
	
	
	/*
	
	calcularPuntuacio (String nomFitxer) – 
	Calcula la puntuació del fitxer JSON proporcionat. 
	Per cada enemic calcularà la seva puntuació, les sumarà, i la tornarà com a int sense decimals (3 punts)
	
	
	La puntuació és cada kill multiplicada pel seu valor base i el seu bonus, i multiplicat per 2 al final si l’enemic és elite
	En l’exemple del Goomba, la puntuació seria: 2*1+2*1.5+2*0.7+2*2+2*0.5 -> 11.4 -> 11

	
	*/
	
	
	//funciona
	public static int calcularPuntuacio (String nomFitxer) throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		
		JSONArray ListaJson = (JSONArray) parser.parse(new FileReader(nomFitxer));
		
		
		
		double acumuladorFinal = 0.0;
	
		for(Object a : ListaJson) {
			JSONObject m_enemic = (JSONObject) a;
			
			JSONArray m_ListaBonusE = (JSONArray) m_enemic.get("bonus");
			Long  mult = (Long) m_enemic.get("puntsBase");
			
			double acumulador = 0.0;
			for (int i = 0; i < m_ListaBonusE.size(); i++) {
				
				if(m_ListaBonusE.get(i) instanceof Long) {
					Long asd = (Long)m_ListaBonusE.get(i);
					acumulador += asd * mult;
				}
				else {
					double asd = (double) m_ListaBonusE.get(i);
					acumulador += asd * mult;
				}
			}
			boolean elite = (boolean)m_enemic.get("elite");
			if(elite) {
				acumulador = acumulador*2;
			}

			acumuladorFinal += acumulador;
		}
		
		
		
		return (int)acumuladorFinal;
		
	}
	
	/*
	
		AfegirPuntuacioEnBaseAPartida(String nomFitxerXML, String nomJugador, String nomFitxerJSON) – 
		Calcula la puntuació del fitxer JSON, i l’afegeix al jugador esmentat. 
		Recalcula totes les posicions en base a la nova puntuació. (1 punt)

	 */
	
	
	public static void AfegirPuntuacioEnBaseAPartida(String nomFitxerXML, String nomJugador, String nomFitxerJSON) throws JAXBException, FileNotFoundException, IOException, ParseException{
		File f = new File(nomFitxerXML);
		JAXBContext context = JAXBContext.newInstance(Partida.class);
		Unmarshaller um = context.createUnmarshaller();
		Partida p = (Partida) um.unmarshal(f);
		
		
		
		
		
		JSONParser parser = new JSONParser();
		JSONArray ListaJson = (JSONArray) parser.parse(new FileReader(nomFitxerJSON));
		
		
		int total = calcularPuntuacio(nomFitxerJSON);
		XMLWriter.afegeixPuntuacio(nomFitxerXML, nomJugador, total);
		
	}
	
	
}
