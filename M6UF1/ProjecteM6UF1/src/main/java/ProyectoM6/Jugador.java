package ProyectoM6;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement(name="jugador")
public class Jugador implements Comparable{
	

	private String nombre;
	private String apellido;
	private String iniciales;
	



	private int posicion;
	private String DNI;
	
	
	private ArrayList<Integer> puntuaciones;
	
	
	// ara miro esto
	Configuracion confPlayer = new Configuracion();

	
	@XmlElement(name="dni")
	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		DNI = dNI;
	}

	public Jugador() {
		super();
	}
	
	public Jugador(String nombre, String iniciales, String DNI) {
		super();
		this.nombre = nombre;
		this.iniciales = iniciales;
		this.DNI = DNI;
		this.puntuaciones = puntuaciones;
	}
	
	@XmlElement(name="nombre")
	public String getNombre() {
		return this.nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	@XmlElement(name="apellido")
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	

	
	@XmlElement(name="iniciales")
	public String getIniciales() {
		return this.iniciales;
	}
	
	public void setIniciales(String iniciales) {
		this.iniciales = iniciales;
	}
	
	
	
	
	//
	
	@XmlElement(name="posicion")
	public int getPosicion() {
		return posicion;
	}

	
	
	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	@XmlElementWrapper(name="puntuaciones")
	@XmlElement(name="puntos")
	public ArrayList<Integer> getPuntuaciones() {
		return puntuaciones;
	}
	
	public void setPuntuaciones(ArrayList<Integer> puntuaciones) {
		this.puntuaciones = puntuaciones;
	}
	
	
	@XmlElement(name="configuracion")
	public Configuracion getConfPlayer() {
		return confPlayer;
	}

	public void setConfPlayer(Configuracion confPlayer) {
		this.confPlayer = confPlayer;
	}

	

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", iniciales=" + iniciales + ", posicion=" + posicion + ", puntuaciones=" + puntuaciones + ", confPlayer=" + confPlayer + "]\n";
	}

	@Override
	public int compareTo(Object arg0) {
		Jugador j = (Jugador) arg0;
		
		if(this.equals(j)) 
			return 0;
		
		else {
			if(this.getPuntuaciones().get(0) > j.getPuntuaciones().get(0)) 
				return 1;
			
			else if(this.getPuntuaciones().get(0) < j.getPuntuaciones().get(0)) 
				return -1;
			
			else {
				if(this.nombre.compareTo(j.nombre) > 0) 
					return 1;
				
				else  
					return -1;
			}
		}
	}
	
	

	
	
	
	
	
}
