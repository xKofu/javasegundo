package ExamenPruebaColonos;

import java.util.ArrayList;
import java.util.HashMap;

import EventManager.Observer;

public class Scheduler {

	private static Scheduler Schedule; 
	
	HashMap<Room, ArrayList<Settler>> List;
	
	private Scheduler() {
		List = new HashMap<Room, ArrayList<Settler>>();
	}
	
	public static Scheduler GetInstance() {
		
		if (Schedule == null) {
			Schedule = new Scheduler();
		}
		
		return Schedule;
		
	}
	
	public void AddRoom(Room r) {
		List.put(r, new ArrayList<Settler>());
	}
	
	public void registerObserver(Room r, Settler s) {
		s.setM_Room(r);
		List.get(r).add(s);
	}
	
	public void unregisterObserver(Room r, Settler s) {
		List.get(r).remove(s);
	}

	public void notifyObservers(Room r) {
		
		for (ArrayList<Settler> AS : List.values()) {
		
			for (Settler settler : AS) {
				
				settler.notifyObserver(r.getM_id());			
				
			}
			
		}
		
		r.MachineAction();
		
	}
	
}
