package ExamenPruebaColonos;

import java.util.ArrayList;

public class Room {

	private ArrayList<Machine> MachineList = new ArrayList<Machine>();
	private static int id = 0;
	private int m_id;
	private String m_name;
	
	public Room(String name) {
		m_id = id;
		id++;
		m_name = name;
	}
	
	public void MachineAction() {
		
		System.out.println("Se ponen en funcionamiento las Maquinas de " + m_name + "!");
		System.out.println();
		
		for (Machine machine : MachineList) {
			machine.MachineAction();
		}
		
		System.out.println();
	}
	
	public void AddMachine(Machine m) {
		
		MachineList.add(m);
		
	}
	
	public void RemoveMachine(Machine m) {
		
		MachineList.remove(m);
	
	}

	public int getM_id() {
		return m_id;
	}

	public void setM_id(int m_id) {
		this.m_id = m_id;
	}
	
	
	
}
