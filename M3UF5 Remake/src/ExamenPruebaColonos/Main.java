package ExamenPruebaColonos;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		Room Medbay = new Room("Medbay");
		Room Engineering = new Room("Engineering");
		Room Maintenance = new Room("Maintenance");
		
		Medbay.AddMachine(new Dispenser());
		Medbay.AddMachine(new Autotreat());
		Medbay.AddMachine(new Laboratory());
		
		Engineering.AddMachine(new Dispenser());
		Engineering.AddMachine(new Laboratory());
		Engineering.AddMachine(new Welding());
		
		Maintenance.AddMachine(new Dispenser());
		Maintenance.AddMachine(new Welding());
		Maintenance.AddMachine(new Broom());
		
		ArrayList<Settler> SettlerList = new ArrayList<Settler>();
	
		for (int i = 0; i < 9; i++) {
			SettlerList.add(new Settler());
		}
		
		Scheduler Schedule = Scheduler.GetInstance();
		
		Schedule.AddRoom(Medbay);
		Schedule.AddRoom(Engineering);
		Schedule.AddRoom(Maintenance);
		
		Schedule.registerObserver(Medbay, SettlerList.get(0));
		Schedule.registerObserver(Medbay, SettlerList.get(1));
		Schedule.registerObserver(Medbay, SettlerList.get(2));
		Schedule.registerObserver(Engineering, SettlerList.get(3));
		Schedule.registerObserver(Engineering, SettlerList.get(4));
		Schedule.registerObserver(Engineering, SettlerList.get(5));
		Schedule.registerObserver(Maintenance, SettlerList.get(6));
		Schedule.registerObserver(Maintenance, SettlerList.get(7));
		Schedule.registerObserver(Maintenance, SettlerList.get(8));
				
		System.out.println(" --> DIA 1 <--");
		
		System.out.println();
		
		Schedule.notifyObservers(Engineering);
		
		System.out.println();
		
		Schedule.unregisterObserver(Medbay, SettlerList.get(0));
		Schedule.unregisterObserver(Medbay, SettlerList.get(1));
		Schedule.unregisterObserver(Medbay, SettlerList.get(2));
		Schedule.unregisterObserver(Engineering, SettlerList.get(3));
		Schedule.unregisterObserver(Engineering, SettlerList.get(4));
		Schedule.unregisterObserver(Engineering, SettlerList.get(5));
		Schedule.unregisterObserver(Maintenance, SettlerList.get(6));
		Schedule.unregisterObserver(Maintenance, SettlerList.get(7));
		Schedule.unregisterObserver(Maintenance, SettlerList.get(8));
		
		Schedule.registerObserver(Medbay, SettlerList.get(6));
		Schedule.registerObserver(Medbay, SettlerList.get(7));
		Schedule.registerObserver(Medbay, SettlerList.get(8));
		Schedule.registerObserver(Engineering, SettlerList.get(3));
		Schedule.registerObserver(Engineering, SettlerList.get(4));
		Schedule.registerObserver(Engineering, SettlerList.get(5));
		Schedule.registerObserver(Maintenance, SettlerList.get(0));
		Schedule.registerObserver(Maintenance, SettlerList.get(1));
		Schedule.registerObserver(Maintenance, SettlerList.get(2));
		
		System.out.println(" --> DIA 2 <--");
		
		System.out.println();
		
		Schedule.notifyObservers(Maintenance);
		
	}

}
