package ExamenPruebaColonos;

public class Settler implements Observer{

	private String m_name;
	private int Room_id;
	private static int id = 0;
	private Room m_Room;
	
	public Settler() {
		
		m_name = "Settler" + id;
		id++;
	
	}
	
	public int getRoom_id() {
		return Room_id;
	}

	public void setRoom_id(int room_id) {
		Room_id = room_id;
	}
	
	public Room getM_Room() {
		return m_Room;
	}

	public void setM_Room(Room m_Room) {
		this.m_Room = m_Room;
	}

	@Override
	public void notifyObserver(int RoomCode) {
	
		if (m_Room.getM_id() == RoomCode) {
			
			System.out.println("Soy" + m_name + " y trabajo en " + m_Room.getM_id());
			//m_Room.MachineAction();
			
		} else {
			
			System.out.println("Chilling...");
			
		}
		
	}

}
