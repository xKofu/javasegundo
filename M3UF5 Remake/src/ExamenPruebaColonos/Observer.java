package ExamenPruebaColonos;

public interface Observer {
	
	public void notifyObserver(int RoomCode);

}
