package EventManager;

public class Test {

	public static void main(String[] args) {
	
		EventManager em = EventManager.getInstance();
		
		em.CrearEvento("Evento1");
		em.CrearEvento("Evento2");
		em.CrearEvento("Evento3");
		
		Cosa c1 = new Cosa();
		Cosa c2 = new Cosa();
		Cosa c3 = new Cosa();
		Cosa c4 = new Cosa();
		Cosa c5 = new Cosa();
		Cosa c6 = new Cosa();
		Cosa c7 = new Cosa();
		Cosa c8 = new Cosa();
		Cosa c9 = new Cosa();
		
		em.registerObserver("Evento1", c1);
		em.registerObserver("Evento1", c2);
		em.registerObserver("Evento1", c3);
		em.registerObserver("Evento2", c4);
		em.registerObserver("Evento3", c4);
		em.registerObserver("Evento2", c5);
		em.registerObserver("Evento2", c6);
		em.registerObserver("Evento2", c7);
		em.registerObserver("Evento3", c8);
		em.registerObserver("Evento3", c1);
		em.registerObserver("Evento3", c6);
		em.registerObserver("Evento2", c2);
		em.registerObserver("Evento1", c3);
		em.registerObserver("Evento3", c7);
		em.registerObserver("Evento3", c9);
		
		em.notifyObservers("Evento1");
		em.notifyObservers("Evento2");
		em.notifyObservers("Evento3");
		
		
	}

}
