package EventManager;

public interface Observer {
	
	public void notifyObserver(String evento);

}
