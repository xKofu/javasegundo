package EventManager;

import java.util.ArrayList;
import java.util.HashMap;

public class EventManager {
	
	public static EventManager em;
	
	HashMap<String, ArrayList<Observer>> lista;
	
	private EventManager() {
		lista = new HashMap<String, ArrayList<Observer>>();
	}
	
	public static EventManager getInstance() {
		
		if (em == null) {
			em = new EventManager();
		}
		
		return em;
	}
	
	public void CrearEvento(String evento) {
		lista.put(evento, new ArrayList<Observer>());
	}
	
	public void registerObserver(String evento, Observer observer) {
		System.out.println(observer + " Registrado a " + evento);
		lista.get(evento).add(observer);
	}
	
	public void unregisterObserver(String evento, Observer observer) {
		System.out.println(observer + " Desregistrado de " + evento);
		lista.get(evento).remove(observer);
	}

	public void notifyObservers(String evento) {
		
		for (int i = lista.get(evento).size()-1; i >= 0; i--) {
			lista.get(evento).get(i).notifyObserver(evento);
		}
	
	}

}

