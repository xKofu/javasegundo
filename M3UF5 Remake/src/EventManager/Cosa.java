package EventManager;

import java.util.Random;

public class Cosa implements Observer {

	public String m_nombre;
	public int m_edad;
	public static int id = 1;
	
	public Cosa() {
		
		m_nombre = "Cosa " + id;
		id++;
		
	}

	@Override
	public String toString() {
		return m_nombre;
	}
	
	@Override
	public void notifyObserver(String evento) {
		
		switch(evento) {
		case "Evento1":
			System.out.println("soy " + m_nombre + " y he hecho " + evento);
			break;
		case "Evento2":
			System.out.println("soy " + m_nombre + " y he hecho " + evento);
			break;
		case "Evento3":
			System.out.println("soy " + m_nombre + " y he hecho " + evento);
			break;
		default:
			System.out.println("Chill");
		}
		
		EventManager.getInstance().unregisterObserver(evento, this);
		
	}	
	
}
