package Activitat1;

import java.util.ArrayList;

public class MyStack<T> {

	ArrayList<T> Lista;
	
	public MyStack() {
		
		Lista = new ArrayList<T>();
		
	}
	
	public T Pop() throws EmptyListException {
			
		int Size =  Lista.size();
			
		if (Size <= 0)
			throw new EmptyListException();
		
		T Pop_Res = Lista.get(Lista.size()-1);
			
		Lista.remove(Lista.size()-1);
			
		return Pop_Res;	
		
				
	}
	
	public void Push(T Element) {
		
		Lista.add(Element);
			
	}

	@Override
	public String toString() {
		return "" + Lista + "";
	}
	
	

}
