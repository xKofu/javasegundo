package Activitat1;

import java.util.ArrayList;

public class MyQueue<T> {

	
ArrayList<T> Lista;
	
	public MyQueue() {
		
		Lista = new ArrayList<T>();
		
	}
	
	public T Pop() throws EmptyListException {
			
		int Size =  Lista.size();
		
		if (Size <= 0)
			throw new EmptyListException();
	
		T Pop_Res = Lista.get(0);
		
		Lista.remove(0);
		
		return Pop_Res;
	}
	
	public void Push(T Element) {
		
		Lista.add(Element);
		
		
	}

	@Override
	public String toString() {
		return "" + Lista + "";
	}
	
}
