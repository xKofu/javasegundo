package Activitat1;

public class MyClassFactory implements Factory<MyClass>{

	private static int m_counter = 0;
	
	@Override
	public MyClass create() {
		
		MyClass NewElement = new MyClass();
		m_counter++;
		return NewElement;
	}

	@Override
	public void destroy(MyClass element) {
		
		
		
	}
	
}
