package Activitat1;

import java.util.ArrayList;
import java.util.Iterator;

public class MyPool<T> {

	ArrayList<Integer> Lista = new ArrayList<Integer>();
	ArrayList<Boolean> Check = new ArrayList<Boolean>();
	
	public MyPool() {
		
		for (int i = 0; i < 10; i++) {
			
			Lista.add(i);
			
		}
		for (int i = 0; i < 10; i++) {
			
			Check.add(true);
			
		}
		
	}
	
	public Integer Get() throws FullPoolException {
		
		for (int i = 0; i < Lista.size(); i++) {
			if(Check.get(i)) {
				Check.set(i, false);
				return Lista.get(i);
			}
		}
		
		throw new FullPoolException();
		
	}
	
	public void Return(Integer Dev) throws MissingValueException {
		
		if (!Lista.contains(Dev))
			throw new MissingValueException();
		
		for (int i = 0; i < Lista.size(); i++) {
			if (Dev == Lista.get(i)) {
				if (!Check.get(i)) {
					Check.set(i, true);
				}
			}
			
		}
		
		
		
	}

	@Override
	public String toString() {
		return "" + Check + "";
	}
	
	
	
}
