package Activitat1;

import java.util.Iterator;

public class Test {

	public static void main(String[] args) {
		
		// Declaracion
		
		MyStack<Integer> TestS = new MyStack<Integer>();
		MyQueue<Integer> TestQ = new MyQueue<Integer>();
		MyPool<Integer> TestP = new MyPool<Integer>();
		MyClassFactory m_factory = new MyClassFactory();
		MyPoolG<MyClass> TestPG = new MyPoolG<MyClass>(m_factory);
		
		
		// Prueba Pool Generica
		
		/*
		try {
			MyClass Prueba1 = TestPG.Get();
			MyClass Prueba2 = TestPG.Get();
			MyClass Prueba3 = TestPG.Get();
			
			System.out.println(TestPG);
			
			TestPG.Return(Prueba2);
			
			System.out.println(TestPG);
			
			MyClass Prueba4 = TestPG.Get();
			MyClass Prueba5 = TestPG.Get();
			
			TestPG.Return(Prueba1);
			
			System.out.println(TestPG);
			
			TestPG.Return(Prueba1);
			
		} catch (FullPoolException | MissingValueException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		*/
		
		// Prueba Stack y Queue				
		
		/*
		TestS.Pop();
		TestQ.Pop();
		
		TestS.Push(1);
		TestS.Push(2);
		TestS.Push(3);
		TestS.Push(4);
		
		TestS.Pop();
		
		System.out.println("Stack:");
		System.out.println(TestS);
		System.out.println();
		
		TestQ.Push(1);
		TestQ.Push(2);
		TestQ.Push(3);
		TestQ.Push(4);
		
		TestQ.Pop();
		
		System.out.println("Queue: ");
		System.out.println(TestQ);
		System.out.println();
		*/
		
		// Prueba Pool Integer
		
		/*
		
		try {
			//TestP.Return(100);
			
			int Prueba1 = TestP.Get();
			int Prueba2 = TestP.Get();
			int Prueba3 = TestP.Get();
			int Prueba4 = TestP.Get();
			//int Prueba5 = TestP.Get();
			//int Prueba6 = TestP.Get();
			//int Prueba7 = TestP.Get();
			//int Prueba8 = TestP.Get();
			//int Prueba9 = TestP.Get();
			//int Prueba10 = TestP.Get();
			//int Prueba11 = TestP.Get();
			
			TestP.Return(Prueba2);
			
			System.out.println(TestP);
			
			int Prueba5 = TestP.Get();
			
			System.out.println(TestP);
			
			TestP.Return(Prueba1);
			TestP.Return(Prueba3);
			
			System.out.println(TestP);
			
			int Prueba6 = TestP.Get();
			
			System.out.println(TestP);
		} catch (FullPoolException e) {
			e.printStackTrace();
		} catch (MissingValueException e) {
			e.printStackTrace();
		}
		
		*/
		
		// Prueba Exceptions
		
		/*
		for (int i = 0; i < 10; i++) {
			TestS.Push(0);
		}
		
		try {
			for (int i = 0; i < 11; i++) {
				System.out.println(TestS.Pop());				
			}
		} catch (EmptyListException e) {
			System.out.println("La lista a la que intentas acceder esta vacia -- Stack");
		}
		
		try {
			TestS.Pop();
		}
		catch(EmptyListException e) {
			System.out.println("nigger's");
		}
		
		try {
			TestQ.Pop();
		} catch (EmptyListException e) {
			System.out.println("La lista a la que intentas acceder esta vacia -- Queue");
		}
		*/
		
		
		
		
	}

}
