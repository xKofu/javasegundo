package Activitat1;

import java.util.ArrayList;

public class MyPoolG<T> {

	ArrayList<T> Lista = new ArrayList<T>();
	Factory<T> factory;
	ArrayList<Boolean> Check = new ArrayList<Boolean>();
	
	public MyPoolG(Factory<T> Fact) {
		
		factory = Fact;
		
		for (int i = 0; i < 10; i++) {
			
			Lista.add(factory.create());
			Check.add(true);
			
		}
		
	}
	
	public T Get() throws FullPoolException {
		
		for (int i = 0; i < Lista.size(); i++) {
			if(Check.get(i)) {
				Check.set(i, false);
				return Lista.get(i);
			}
		}
		
		throw new FullPoolException();
		
	}
	
	public void Return(T element) throws MissingValueException {
		
		if (!Lista.contains(element))
			throw new MissingValueException();
		
		for (int i = 0; i < Lista.size(); i++) {
			if (element == Lista.get(i)) {
				if (!Check.get(i)) {
					Check.set(i, true);
				}
			}
			
		}
		
		
		
	}

	@Override
	public String toString() {
		return "" + Check + "";
	}
	
}
