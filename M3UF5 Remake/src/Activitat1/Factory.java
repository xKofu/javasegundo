package Activitat1;

public interface Factory<T>
{
	public T create();
	public void destroy(T element);
}
