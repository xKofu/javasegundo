package PruebaObserversExtra;

public interface Observer {
	
	public void notifyObserver(String nombre);

}
