package PruebaObserversExtra;

import java.util.ArrayList;

public class Observable {
	
	ArrayList<Observer> observers = new ArrayList<Observer>();
	
	public void registerObserver(Observer observer) {
		observers.add(observer);
	}
	
	public void unregisterObserver(Observer observer) {
		observers.remove(observer);
	}

	public void notifyObservers(String nombre) {
		/*
		for(Observer observer: observers) {	
			observer.notifyObserver(nombre);
		}	
		*/
		for (int i = observers.size()-1; i >= 0; i--) {
			observers.get(i).notifyObserver(nombre);
		}
	}

}

