package PruebaObserversExtra;

import java.util.Random;

public class Paciente implements Observer {

	Random r = new Random();
	
	public String m_nombre;
	public int m_edad;
	public static int id = 1;
	public Consulta m_Cons;
	
	
	public Paciente(Consulta cons) {
		
		m_nombre = "Paciente " + id;
		id++;
		m_edad = r.nextInt(5)+20;
		m_Cons = cons;
		
		m_Cons.registerObserver(this);
	}


	@Override
	public String toString() {
		return m_nombre;
	}


	@Override
	public void notifyObserver(String nombre) {
		
		if (m_nombre.equals(nombre)) {
			System.out.println("Hey, soy " + m_nombre);
			//m_Cons.getBorrar().add(this);
			m_Cons.unregisterObserver(this);
		} else {
			System.out.println();
		}
		
	}
		
	
}
