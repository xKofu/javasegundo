package PruebaObserversExtra;

import java.util.ArrayList;

public class TestObservers {

	public static void main(String[] args) {
		
		Consulta c = new Consulta();
		ArrayList<Observer> Pacientes = new ArrayList<Observer>();
		
		for(int i=0; i<10; i++)
		{
			
			Paciente x = new Paciente(c);
			//Pacientes.add(x);
			//c.registerObserver(x);
			System.out.println(x);
			
		}
		
		System.out.println(c);
		
		c.notifyObservers("Paciente 3");

		System.out.println(c);
		
		c.notifyObservers("Paciente 4");
		c.notifyObservers("Paciente 3");
		
		System.out.println(c);
		
	}
	
}
