package Activitat3Parte1;

import java.util.ArrayList;

public class Spell {

	private ArrayList<Element> Elements;
	
	public Spell() {
		Elements = new ArrayList<Element>();
	}
	
	public void Cast() {
		
		for (Element e : Elements) {
			e.Cast();
		}
		
	}

	public void AddElement(Element e) {
		
		Elements.add(e);
		
	}
	
	public ArrayList<Element> getElements() {
		return Elements;
	}

	public void setElements(ArrayList<Element> elements) {
		Elements = elements;
	}
	
	
	
}
