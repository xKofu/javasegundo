package MaquinaDeEstados;

import java.util.ArrayList;

public class StateMachine {

	private ArrayList<State> StateList;
	
	private State ActualState;
	
	public StateMachine() {
		
		StateList =  new ArrayList<State>();
		ActualState = null;
	}
	
	public void addState(State state) {
		StateList.add(state);
	}
	
	public void removeState(State state) {
		StateList.remove(state);
	}
	
	public <T extends State> void setState(Class<T> stateClass) {
		
		
		for (State state : StateList) {
			
			if (stateClass.isInstance(state)) {
				
				if (ActualState != null)
					ActualState.Stop();
				
				ActualState = state;
					
				ActualState.Start();
				
				ActualState.Update();
				
			}
			
		}
		
		
		
	}
	
}
