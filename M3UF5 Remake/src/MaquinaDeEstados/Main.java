package MaquinaDeEstados;

import MaquinaDeEstados.Estados.BlueState;
import MaquinaDeEstados.Estados.GreenState;
import MaquinaDeEstados.Estados.RedState;
import MaquinaDeEstados.Estados.YellowState;

public class Main {

	public static void main(String[] args) {
		
		StateMachine Hey = new StateMachine();
		
		Hey.addState(new BlueState());
		Hey.addState(new RedState());
		Hey.addState(new YellowState());
		Hey.addState(new GreenState());

		Hey.setState(BlueState.class);
		Hey.setState(GreenState.class);
		Hey.setState(BlueState.class);
		Hey.setState(RedState.class);
	
	}

}
