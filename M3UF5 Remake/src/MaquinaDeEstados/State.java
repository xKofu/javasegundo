package MaquinaDeEstados;

public abstract class State {

	public abstract void Start();
	
	public abstract void Stop();
	
	public abstract void Update();
		
}
