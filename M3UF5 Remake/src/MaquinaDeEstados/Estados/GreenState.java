package MaquinaDeEstados.Estados;

import MaquinaDeEstados.State;

public class GreenState extends State {

	private String m_name = this.getClass().getSimpleName();
	
	@Override
	public void Start() {
		System.out.println(" Green State Start ");
	}

	@Override
	public void Stop() {
		System.out.println(" Green State Stop ");
	}

	@Override
	public void Update() {
		System.out.println(" --- Green State Running --- ");
	}

}
