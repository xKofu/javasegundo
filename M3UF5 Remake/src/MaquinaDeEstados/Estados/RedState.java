package MaquinaDeEstados.Estados;

import MaquinaDeEstados.State;

public class RedState extends State {

	private String m_name = this.getClass().getSimpleName();
	
	@Override
	public void Start() {
		System.out.println(" Red State Start ");
	}

	@Override
	public void Stop() {
		System.out.println(" Red State Stop ");
	}

	@Override
	public void Update() {
		System.out.println(" --- Red State Running --- ");
	}

}
