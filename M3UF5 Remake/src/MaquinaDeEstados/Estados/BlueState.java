package MaquinaDeEstados.Estados;

import MaquinaDeEstados.State;

public class BlueState extends State {

	private String m_name = this.getClass().getSimpleName();
	
	@Override
	public void Start() {
		System.out.println(" Blue State Start ");
	}

	@Override
	public void Stop() {
		System.out.println(" Blue State Stop ");
	}

	@Override
	public void Update() {
		System.out.println(" --- Blue State Running --- ");
	}

}
