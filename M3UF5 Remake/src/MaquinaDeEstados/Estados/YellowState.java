package MaquinaDeEstados.Estados;

import MaquinaDeEstados.State;

public class YellowState extends State {

	private String m_name = this.getClass().getSimpleName();
	
	@Override
	public void Start() {
		System.out.println(" Yellow State Start ");
	}

	@Override
	public void Stop() {
		System.out.println(" Yellow State Stop ");
	}

	@Override
	public void Update() {
		System.out.println(" --- Yellow State Running --- ");
	}

}
