package GameLoop;

import java.util.ArrayList;

public class MyPoolG<T> {

	ArrayList<T> Lista = new ArrayList<T>();
	Factory<T> factory;
	ArrayList<Boolean> Check = new ArrayList<Boolean>();
	
	public MyPoolG(Factory<T> Fact, int tama�o) {
		
		factory = Fact;
		
		for (int i = 0; i < tama�o; i++) {
			
			Lista.add(factory.create());
			Check.add(true);
			
		}
		
	}
	
	public T GetElement() throws FullPoolException {
		
		for (int i = 0; i < Lista.size(); i++) {
			if(Check.get(i)) {
				Check.set(i, false);
				return Lista.get(i);
			}
		}
		
		throw new FullPoolException();
		
	}
	
	public boolean tryReturnElement(T element) throws MissingValueException {
		
		if (!Lista.contains(element)) {
			return false;
		}
		
		for (int i = 0; i < Lista.size(); i++) {
			if (element == Lista.get(i)) {
				if (!Check.get(i)) {
					Check.set(i, true);
					return true;
				}
			}
			
		}
		
		return false;
		
	}

	@Override
	public String toString() {
		return "" + Check + "";
	}
	
}
