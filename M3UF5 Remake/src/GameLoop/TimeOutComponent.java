package GameLoop;

import java.util.Random;

public class TimeOutComponent implements Component {

	private int tiempoDeVida = 0;
	private double Cont = 0;
	private Random r = new Random();
	
	public TimeOutComponent() {
		
			tiempoDeVida = r.nextInt(5)+1;
			Cont = tiempoDeVida;
			
	}
	
	@Override
	public void update(double DeltaTime) {
	
 		Cont -= DeltaTime;
		
	}

	@Override
	public void render() throws FullPoolException, MissingValueException {
	
		if ((int) Cont != tiempoDeVida) {
			tiempoDeVida = (int) Cont;
			System.out.println(" -> Quedan " + tiempoDeVida + " segundos de vida!");
		}
		
		if (tiempoDeVida <= 0) {
			System.out.println(" --- TimeOut acaba --- ");
			Engine.GetInstance().getTimeOutPool().tryReturnElement(this);
//			Engine.GetInstance().RemoveGameObject(this);
		}
		
	}

}
