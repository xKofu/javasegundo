package GameLoop;

import java.util.ArrayList;
import java.util.List;

public interface GameObject {

	List<Component> ComponentList = new ArrayList<Component>();
	void update(double DeltaTime);
	void render() throws FullPoolException, MissingValueException;
	void addComponent(Component component);
	void removeComponent(Component component);
	
}
