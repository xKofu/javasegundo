package GameLoop;

public class Main {

	public static void main(String[] args) throws FullPoolException, MissingValueException {

		int fps = 60;

		int frameTime = 1000 / fps;

		Engine E = Engine.GetInstance();

		E.init();

		double deltaTime = 0;

		Long lastFrame = System.nanoTime();

		while (true) {

			deltaTime = (System.nanoTime() - lastFrame) / 1000000000d;

			E.input();
			E.update(deltaTime);
			E.render();

			lastFrame = System.nanoTime();

			try {
				Thread.sleep(frameTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
