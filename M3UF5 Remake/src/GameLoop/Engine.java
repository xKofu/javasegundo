package GameLoop;

import java.sql.Time;
import java.util.ArrayList;

import GameLoop.GameObjects.Spawner;
import GameLoop.GameObjects.TimeOut;
import GameLoop.GameObjects.TimeOutFactory;
import GameLoop.GameObjects.Timer;

public class Engine {

	private ArrayList<GameObject> GOList = new ArrayList<GameObject>();
	private TimeOutFactory TF;
	private MyPoolG<TimeOutComponent> TimeOutPool = new MyPoolG<TimeOutComponent>(TF, 30);
	private static Engine e; 
	
	public static Engine GetInstance() {
		
		if (e == null) 
			e = new Engine();
		
		return e;
	}
	
	public void init() {

		GOList.add(new Timer());
		GOList.add(new Spawner());
	}
	
	public void AddGameObject(GameObject GO) {
		GOList.add(GO);
	}
	
	public void RemoveGameObject(GameObject GO) {
		GOList.remove(GO);
	}

	public void update(double deltaTime) {
		for(int i = GOList.size()-1; i >= 0; i--) {
			GOList.get(i).update(deltaTime);
		}
		
	}

	public void render() throws FullPoolException, MissingValueException {
		for(int i = GOList.size()-1; i >= 0; i--) {
			GOList.get(i).render();
		}
		
		
	}

	public void input() {
		
	}

	public MyPoolG<TimeOutComponent> getTimeOutPool() {
		return TimeOutPool;
	}
	
	

}
