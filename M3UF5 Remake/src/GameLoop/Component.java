package GameLoop;

public interface Component {

	void update(double DeltaTime);
	void render() throws FullPoolException, MissingValueException;
	
}
