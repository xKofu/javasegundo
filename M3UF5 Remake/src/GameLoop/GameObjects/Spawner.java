package GameLoop.GameObjects;

import GameLoop.Component;
import GameLoop.Engine;
import GameLoop.FullPoolException;
import GameLoop.GameObject;
import GameLoop.MyPoolG;

public class Spawner implements GameObject{

//	private double Cont = 0;
	
	@Override
	public void update(double DeltaTime) {
		
//		Cont += DeltaTime;
		for (Component component : ComponentList) {
			component.update(DeltaTime);
		}
	}

	@Override
	public void render() throws FullPoolException {
		
//		if (Cont >= 5) {
//			
//			Engine.GetInstance().AddGameObject(Engine.GetInstance().getTimeOutPool().GetElement());
//			Cont = 0;
//			System.out.println("TimeOut Generado");
//			
//		}
		for (Component component : ComponentList) {
			component.render();
		}
		
	}

	@Override
	public void addComponent(Component component) {
		this.ComponentList.add(component);
	}

	@Override
	public void removeComponent(Component component) {
		this.ComponentList.remove(component);
	}

	
	
}
