package GameLoop.GameObjects;

import GameLoop.Component;
import GameLoop.FullPoolException;
import GameLoop.GameObject;

public class Timer implements GameObject{

	double Timer = 0;
	
	int LastTimer = -1;
	
	@Override
	public void update(double DeltaTime) {
		
		Timer += DeltaTime;
		for (Component component : ComponentList) {
			component.update(DeltaTime);
		}
	
	}

	@Override
	public void render() throws FullPoolException {
		
		if ((int) Timer != LastTimer) {
			LastTimer = (int) Timer;
			System.out.println(LastTimer);
		}
		for (Component component : ComponentList) {
			component.render();
		}
		
	}

	@Override
	public void addComponent(Component component) {
		this.ComponentList.add(component);
	}

	@Override
	public void removeComponent(Component component) {
		this.ComponentList.remove(component);
	}

}
