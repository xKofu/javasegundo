package GameLoop.GameObjects;

import GameLoop.Factory;
import GameLoop.TimeOutComponent;

public class TimeOutFactory implements Factory<TimeOutComponent> {

		private static int m_counter = 0;

		@Override
		public TimeOutComponent create() {
			TimeOutComponent NewElement = new TimeOutComponent();
			m_counter++;
			return NewElement;
		}

		@Override
		public void destroy(TimeOutComponent element) {
			// TODO Auto-generated method stub
			
		}

}

