package GameLoop.GameObjects;

import GameLoop.Component;
import GameLoop.Engine;
import GameLoop.FullPoolException;
import GameLoop.GameObject;
import GameLoop.TimeOutComponent;

public class SpawnerComponent implements Component {

	private double Cont = 0;
	private GameObject myGo = null; 
	
	@Override
	public void update(double DeltaTime) {
		
		Cont += DeltaTime;
		
	}

	@Override
	public void render() throws FullPoolException {
		
		if (Cont >= 5) {
			
			TimeOutComponent toc = Engine.GetInstance().getTimeOutPool().GetElement();
			
//			Engine.GetInstance().AddGameObject(Engine.GetInstance().getTimeOutPool().GetElement());
			Cont = 0;
			System.out.println("TimeOut Generado");
			
		}
		
	}
	
	

}
