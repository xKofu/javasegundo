package GameLoop.GameObjects;

import java.util.Random;

import GameLoop.Component;
import GameLoop.Engine;
import GameLoop.FullPoolException;
import GameLoop.GameObject;
import GameLoop.MissingValueException;

public class TimeOut implements GameObject{

//	private int tiempoDeVida = 0;
//	private double Cont = 0;
//	private Random r = new Random();
	
//	public TimeOut() {
//	
//		tiempoDeVida = r.nextInt(5)+1;
//		Cont = tiempoDeVida;
//		
//	}
	
 	@Override
	public void update(double DeltaTime) {
		
// 		Cont -= DeltaTime;
 		for (Component component : ComponentList) {
			component.update(DeltaTime);
		}
 		
	}

	@Override
	public void render() throws MissingValueException, FullPoolException {
				
//		if ((int) Cont != tiempoDeVida) {
//			tiempoDeVida = (int) Cont;
//			System.out.println(" -> Quedan " + tiempoDeVida + " segundos de vida!");
//		}
//		
//		if (tiempoDeVida <= 0) {
//			System.out.println(" --- TimeOut acaba --- ");
//			Engine.GetInstance().getTimeOutPool().tryReturnElement(this);
//			Engine.GetInstance().RemoveGameObject(this);
//		}
		for (Component component : ComponentList) {
			component.render();
		}
		
	}

	@Override
	public void addComponent(Component component) {
		this.ComponentList.add(component);
	}

	@Override
	public void removeComponent(Component component) {
		this.ComponentList.remove(component);
	}

}
