package Activitat3parte2;

import java.util.ArrayList;

public class GameObject {

	private String m_name;
	private ArrayList<Component> Components;
	static private int id = 1;
	
	
	public GameObject() {
		
		m_name = "GO " + id;
		id++;
		Components = new ArrayList<Component>();
				
	}
	
	public void AddComponent(Component p) {
	
		Components.add(p);
		
	}
	
	public <T extends Component> boolean HasComponent(Class<T> c) {
		
		for (Component component : Components) 
			if (c.isInstance(component)) 
				return true;
		
		return false;	
		
	}
	
	public <T extends Component> T GetComponent(Class<T> c) {

		for (Component component : Components) 
			if (c.isInstance(component)) 
				return (T) component;
		
		return null;
			
	}
	
}
