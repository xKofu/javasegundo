package Activitat2;

import java.util.ArrayList;
import java.util.Iterator;

public class MyQueue<T> implements Iterable<T> {

	ArrayList<T> Lista;

	public MyQueue() {

		Lista = new ArrayList<T>();

	}

	public T Pop() throws EmptyListException {

		int Size = Lista.size();

		if (Size <= 0)
			throw new EmptyListException();

		T Pop_Res = Lista.get(0);

		Lista.remove(0);

		return Pop_Res;
	}

	public void Push(T Element) {

		Lista.add(Element);

	}

	@Override
	public String toString() {
		return "" + Lista + "";
	}

	private class MyQueueIterator<T> implements Iterator<T> {

		int contador = 0;
		
		@Override
		public boolean hasNext() {
			
			if (contador < Lista.size())
				return true;
			
			return false;
		}

		@Override
		public T next() {
			contador++;
			return (T) Lista.get(contador);
		}

	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new MyQueueIterator<T>();

	}
}
