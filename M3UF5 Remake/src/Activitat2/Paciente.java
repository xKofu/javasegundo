package Activitat2;

import java.util.Random;

public class Paciente implements Observer {

	Random r = new Random();
	
	public String m_nombre;
	public int m_edad;
	public static int id = 1;
	
	
	public Paciente() {
		
		m_nombre = "Paciente " + id;
		id++;
		m_edad = r.nextInt(5)+20;
		
	}


	@Override
	public String toString() {
		return m_nombre;
	}


	@Override
	public boolean notifyObserver(String nombre) {
		
		if (m_nombre.equals(nombre)) {
			System.out.println("Hey, soy " + m_nombre);
			return true;
		}
		
		return false;
		
	}
		
	
}
