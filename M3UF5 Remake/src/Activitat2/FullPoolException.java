package Activitat2;

public class FullPoolException extends Exception{

	public FullPoolException() {
		super("No hay disponibilidad en la Pool");
	}
	
}
