package Activitat2;

import java.util.ArrayList;

public class Observable {
	
	ArrayList<Observer> observers = new ArrayList<Observer>();
	ArrayList<Observer> borrar = new ArrayList<Observer>();
	
	public void registerObserver(Observer observer)
	{
		observers.add(observer);
	}
	
	public void unregisterObserver(Observer observer)
	{
		observers.remove(observer);
	}

	public void notifyObservers(String nombre)
	{
		for(Observer observer: observers) {
			
			boolean b = observer.notifyObserver(nombre);
			
			if (b)
				borrar.add(observer);
			
		}
		
		for (int i = 0; i < borrar.size(); i++) {
			
			unregisterObserver(borrar.get(i));
			
		}
		
		borrar.clear();
			
	}

}

