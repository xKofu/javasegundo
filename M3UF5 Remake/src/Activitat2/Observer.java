package Activitat2;

public interface Observer {
	
	public boolean notifyObserver(String nombre);

}
