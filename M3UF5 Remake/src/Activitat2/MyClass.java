package Activitat2;

public class MyClass implements Comparable{

	private String m_name;
	private String m_surname;
	private int m_age;
	
	public MyClass() {
		
	}
	
	public MyClass(String name) {
		
		m_name = name;
		
	}
	
	
	public MyClass(String m_name, String m_surname, int m_age) {
		this.m_name = m_name;
		this.m_surname = m_surname;
		this.m_age = m_age;
	}

	

	@Override
	public String toString() {
		return "MyClass [m_name=" + m_name + ", m_surname=" + m_surname + ", m_age=" + m_age + "]";
	}

	@Override
	public int compareTo(Object o) {
		MyClass Aux = (MyClass) o;
		
		if (this.equals(Aux)) {
			return 0;
		} else {
			if (this.m_surname.compareTo(Aux.m_surname) < 0) {
				return -1;
			} else if (this.m_surname.compareTo(Aux.m_surname) > 0) {
				return 1;
			} else {
				if (this.m_name.compareTo(Aux.m_name) < 0) {
					return -1;
				} else if (this.m_name.compareTo(Aux.m_name) > 0) {
					return 1;
				} else {
					if (this.m_age > Aux.m_age) {
						return 1;
					} else {
						return -1;
					}
				}
			}
		}
		
	}

	
	
	
	
	
}
