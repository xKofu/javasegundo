package Activitat2;

public class MissingValueException extends Exception{

	public MissingValueException() {
		super("Valor no existente en la Pool");
	}
	
}
