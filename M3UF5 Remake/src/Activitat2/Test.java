package Activitat2;

import java.util.ArrayList;
import java.util.Collections;

public class Test {

	public static void main(String[] args) {
		
		ArrayList<MyClass> TestList = new ArrayList<MyClass>();
		
		TestList.add(new MyClass("Gerard", "Lastres", 20));
		TestList.add(new MyClass("Marta", "Brustenga", 20));
		TestList.add(new MyClass("Pau", "Martinez", 19));
		TestList.add(new MyClass("Mario", "Nocelo", 19));
		TestList.add(new MyClass("Eric", "Noguera", 30));
		
		System.out.println(TestList);
		
		Collections.sort(TestList);
		
		System.out.println(TestList);
		
	}

}
