package MaquinaDeEstadosStack;

import java.util.ArrayList;

import MaquinaDeEstados.State;
import MaquinaDeEstados.StateMachine;

public class StateMachineStack {

	
	ArrayList<State> MyStates = new ArrayList<>();
	MyStack<State> MyStateStack = new MyStack<State>();
	
	
	
	public void addState(State state) {
		MyStates.add(state);
	}
	
	public <T extends State> void setState(Class<T> state) throws EmptyListException {
		
		if (MyStateStack.Size() >= 1) {
			
			for (State s : MyStates) {
				
				if (state.isInstance(s)) {
					
					State auxState = MyStateStack.Pop();
					auxState.Stop();
					
					MyStateStack.Push(s);
					
					MyStateStack.Peek().Start();
					
					MyStateStack.Peek().Update();
					
					return;
				}
				
			}
						
		} else {
			
			pushState(state);
			
		}
		
	}
	
	public <T extends State> void pushState(Class<T> state) {
		
		for (State s : MyStates) {
			
			if (state.isInstance(s)) {
				
				if (MyStateStack.Size() > 0)
					MyStateStack.Peek().Stop();
									
				MyStateStack.Push(s);
				
				MyStateStack.Peek().Start();
				
				MyStateStack.Peek().Update();
				
			}
			
		}
		
	}
	
	public void popState() throws EmptyListException {
		
		if (MyStateStack.Size() > 0) {
			
			MyStateStack.Peek().Stop();
			MyStateStack.Pop();
			
			if (MyStateStack.Size() > 0) {
				
				MyStateStack.Peek().Start();
				MyStateStack.Peek().Update();
			}
		
		}
		
		
		
	}
	
}
