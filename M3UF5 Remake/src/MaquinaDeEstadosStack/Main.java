package MaquinaDeEstadosStack;

import MaquinaDeEstados.Estados.BlueState;
import MaquinaDeEstados.Estados.GreenState;
import MaquinaDeEstados.Estados.RedState;
import MaquinaDeEstados.Estados.YellowState;

public class Main {

	public static void main(String[] args) throws EmptyListException {
		
		StateMachineStack SM = new StateMachineStack();
		
		SM.addState(new BlueState());
		SM.addState(new RedState());
		SM.addState(new YellowState());
		SM.addState(new GreenState());
	
		SM.setState(BlueState.class);
		SM.setState(RedState.class);

		SM.pushState(YellowState.class);
		SM.popState();
		
		SM.pushState(GreenState.class);
		
		SM.setState(BlueState.class);
		
		SM.popState();
		SM.popState();
		SM.popState();
		
	}

}
