package MaquinaDeEstadosStack;

import java.util.ArrayList;
import java.util.Iterator;

public class MyStack<T> implements Iterable<T>{

	ArrayList<T> Lista;
	
	public MyStack() {
		
		Lista = new ArrayList<T>();
		
	}
	
	public T Pop() throws EmptyListException {
			
		int Size =  Lista.size();
			
		if (Size <= 0)
			throw new EmptyListException();
		
		T Pop_Res = Lista.get(Lista.size()-1);
			
		Lista.remove(Lista.size()-1);
			
		return Pop_Res;	
		
				
	}
	
	public void Push(T Element) {
		
		Lista.add(Element);
			
	}
	
	public T Peek() {
		
		if (Lista.size() <= 0)
			return null;
		
		return Lista.get(Lista.size()-1);
		
	}

	@Override
	public String toString() {
		return "" + Lista + "";
	}

	private class MyStackIterator<T> implements Iterator<T> {

		int contador = 0;
		int conta2 = Lista.size()-1;
		
		@Override
		public boolean hasNext() {

			if (contador < Lista.size())
				return true;
			
			return false;
		}

		@Override
		public T next() {
			contador++;
			conta2--;
			return (T) Lista.get(conta2);
		}
		
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new MyStackIterator<T>();
	}
	
	public int Size() {
		return Lista.size();
	}
	

}
